webpackJsonp([29],{

/***/ 139:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddprogramPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddprogramPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddprogramPage = /** @class */ (function () {
    function AddprogramPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadData();
        this.time = new Date(this.navParams.get('date')).toISOString();
        this.time2 = new Date(this.navParams.get('date')).toISOString();
        console.log(this.time);
        this.person21 = '0';
        this.person22 = '0';
        this.person23 = '0';
    }
    AddprogramPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddprogramPage');
    };
    AddprogramPage.prototype.loadData = function () {
        var _this = this;
        var data;
        this.datas = this.http.get('https://zonepage.gr/gymapp/appcrm/getclasses.php');
        this.datas.subscribe(function (result) {
            _this.items = result;
        });
    };
    AddprogramPage.prototype.addprogram = function () {
        var _this = this;
        if (this.yes == true) {
            this.repeat = "1";
        }
        else {
            this.repeat = "0";
        }
        var term = this.items.filter(function (person) { return person.id == _this.category; });
        console.log(term[0].title);
        var gymperson;
        if (this.person == '1') {
            gymperson = 'Γιάννης';
        }
        if (this.person == '2') {
            gymperson = 'Γιώργος';
        }
        if (this.person == '3') {
            gymperson = 'Νίκος';
        }
        if (this.person == '4') {
            gymperson = 'Ελένα';
        }
        if (this.person == '5') {
            gymperson = 'Ξένια';
        }
        var url = 'https://zonepage.gr/gymapp/appcrm/addprogram.php';
        var postData = new FormData();
        postData.append('programtitle', term[0].title);
        postData.append('numclients', this.numclients);
        postData.append('credits', this.credits);
        postData.append('category', this.category);
        postData.append('person', gymperson);
        postData.append('timehour', this.time);
        postData.append('repeat', this.repeat);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
            console.log(data);
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */]);
    };
    AddprogramPage.prototype.addprogram2 = function () {
        var _this = this;
        if (this.yes == true) {
            this.repeat = "1";
        }
        else {
            this.repeat = "0";
        }
        var term = this.items.filter(function (person) { return person.id == _this.category2; });
        console.log(term[0].title);
        var url = 'https://zonepage.gr/gymapp/appcrm/addprogramm2.php';
        var postData = new FormData();
        postData.append('programtitle', term[0].title);
        postData.append('numclients', this.numclients2);
        postData.append('credits', this.credits2);
        postData.append('category', this.category2);
        postData.append('person1', this.person21);
        postData.append('person2', this.person22);
        postData.append('person3', this.person23);
        postData.append('timehour', this.time2);
        postData.append('repeat', this.repeat);
        this.data2 = this.http.post(url, postData);
        this.data2.subscribe(function (data) {
            if (data == "error") {
            }
            console.log(data);
        });
    };
    AddprogramPage.prototype.weekcalendar = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */]);
    };
    AddprogramPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-addprogram',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/addprogram/addprogram.html"*/'<!--\n  Generated template for the AddprogramPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n <ion-buttons start>\n<button ion-button  primary end (click)="weekcalendar()">Back</button>\n\n</ion-buttons>\n\n  <ion-navbar>\n    <ion-title>Add New Program</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n<h2 text-center no-padding no-margin>New Version</h2>\n\n\n<ion-list>\n  \n\n\n\n  <ion-item>\n    <ion-label color="primary">Αριθμός Ατόμων ανά γυμναστη</ion-label>\n    <ion-input type="number" placeholder="Number Input with no label" [(ngModel)]="numclients2"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="primary">Κόστος Credits</ion-label>\n    <ion-input type="number" placeholder="Credits" [(ngModel)]="credits2"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="primary">Επιλογή Κατηγορίας</ion-label>\n  <ion-select [(ngModel)]="category2">\n    <ion-option *ngFor="let field of items" value = "{{field.id}}">{{field.title}}</ion-option>\n    </ion-select>\n\n  </ion-item>\n\n  <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή 1</ion-label>\n  <ion-select [(ngModel)]="person21">\n    <ion-option value="0">Κανένας</ion-option>\n    <ion-option value="1">Γιάννης</ion-option>\n    <ion-option value="2">Γιώργος</ion-option>\n    <ion-option value="3">Νίκος</ion-option>\n    <ion-option value="4">Έλενα</ion-option>\n	  <ion-option value="5">Ξένια</ion-option>\n    <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n   <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή 2</ion-label>\n  <ion-select [(ngModel)]="person22">\n  	<ion-option value="0">Κανένας</ion-option>\n    <ion-option value="1">Γιάννης</ion-option>\n    <ion-option value="2">Γιώργος</ion-option>\n    <ion-option value="3">Νίκος</ion-option>\n    <ion-option value="4">Έλενα</ion-option>\n	  <ion-option value="5">Ξένια</ion-option>\n      <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n   <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή 3</ion-label>\n  <ion-select [(ngModel)]="person23">\n  	<ion-option value="0">Κανένας</ion-option>\n    <ion-option value="1">Γιάννης</ion-option>\n    <ion-option value="2">Γιώργος</ion-option>\n    <ion-option value="3">Νίκος</ion-option>\n    <ion-option value="4">Έλενα</ion-option>\n	  <ion-option value="5">Ξένια</ion-option>\n      <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n <ion-item>\n  <ion-label>Επιλογή Ώρας</ion-label>\n  <ion-datetime displayFormat="DD/MM/YYYY HH:mm" [(ngModel)]="time2"></ion-datetime>\n</ion-item>\n\n \n   <ion-item>\n    <ion-label>Επαναλανβάνετε;</ion-label>\n    <ion-checkbox [(ngModel)]="yes"></ion-checkbox>\n  </ion-item>\n\n  \n</ion-list>\n\n<button ion-button (click)="addprogram2()">Προσθήκη</button>\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/addprogram/addprogram.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], AddprogramPage);
    return AddprogramPage;
}());

//# sourceMappingURL=addprogram.js.map

/***/ }),

/***/ 140:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RequestforuserPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__editprogram_editprogram__ = __webpack_require__(173);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
* Generated class for the RequestforuserPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
var RequestforuserPage = /** @class */ (function () {
    function RequestforuserPage(navCtrl, navParams, http, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.isenabled = false;
        this.datass = JSON.parse(this.navParams.get('progid'));
        this.datas = this.datass[0];
        this.price = this.datas.costcredits;
        this.loadData();
        this.gettrainers(this.datas.progid);
        this.getusercome();
        if (this.datas.category != 2) {
            this.program = 0;
        }
    }
    RequestforuserPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RequestforuserPage');
    };
    //LOad All Customers
    RequestforuserPage.prototype.loadData = function () {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('category', this.datas.category);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/getcustomersforreg.php', postData);
        data.subscribe(function (result) {
            _this.items = result;
            console.log(_this.items);
            _this.total = result.length;
        });
    };
    //Get the trainers of current program
    RequestforuserPage.prototype.gettrainers = function (prog) {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('program', prog);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/gettrainers.php', postData);
        data.subscribe(function (result) {
            _this.trainers = result;
            if (_this.trainers.lenght = 1) {
                _this.selectedItem = _this.trainers[0].dailyperid;
            }
        });
    };
    //Register User Alert
    RequestforuserPage.prototype.registeralert = function (items) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε κλεισετε ραντεβού στον ' + items.lastname + ' στο πρόγραμμα ' + this.datas.tilte,
            buttons: [
                {
                    text: 'NO',
                    handler: function () { }
                },
                {
                    text: 'YES',
                    handler: function () { _this.register(); }
                }
            ]
        });
        confirm.present();
    };
    RequestforuserPage.prototype.registerto = function (item, proid, progra, client) {
        console.log(item, proid, progra, client);
    };
    //Register User to Program
    RequestforuserPage.prototype.register = function () {
        var _this = this;
        console.log(this.program);
        var data;
        var url = 'https://zonepage.gr/gymapp/requestprod2.php';
        var postData = new FormData();
        postData.append('productid', this.datas.progid);
        postData.append('customerid', this.selectedClient);
        postData.append('personid', this.selectedItem);
        postData.append('credits', this.price);
        postData.append('programtype', this.program);
        postData.append('comments', this.comments);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Alert 	
            var alert = _this.alertCtrl.create({
                title: _this.datas.tilte,
                subTitle: result,
                buttons: ['OK']
            });
            alert.present();
            var reminder;
            //Add reminder		 
            if (result == 'Registration Complete') {
                _this.getusercome();
                _this.gettrainers(_this.datas.progid);
            }
        });
    };
    //Delete the Current Program
    RequestforuserPage.prototype.weekcalendar = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */]);
    };
    //Get Users who Came to Program
    RequestforuserPage.prototype.getusercome = function () {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('program', this.datas.progid);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/usersinprogram2.php', postData);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    //Search Function
    RequestforuserPage.prototype.getItems = function (ev) {
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                var usertext;
                usertext = item.lastname + item.fullname + item.email + item.mobilephone;
                return (usertext.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            this.loadData();
        }
    };
    //Delete User Register Alert
    RequestforuserPage.prototype.deleteprogram = function (item) {
        var _this = this;
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε να ακυρώσετε το ραντεβού για ' + name,
            message: 'Επιλέγοντα "ΝΑΙ" επιστρέφονται στον πελάτη τα μισά Credits. Επιλέγοντας ναι με επιστροφή Credits επιστρέφονται όλα τα Credits',
            buttons: [
                { text: 'NO',
                    handler: function () { } },
                { text: 'ΝΑΙ μισα Credits ',
                    handler: function () {
                        _this.returncredits = (parseInt(_this.price) / 2).toFixed(0);
                        _this.deleteregister(item, _this.returncredits);
                        console.log(_this.returncredits);
                    } },
                { text: 'ΝΑΙ Επιστροφή Credits',
                    handler: function () {
                        _this.deleteregister(item, _this.price);
                    } },
                { text: 'Να μην Επιστραφούν',
                    handler: function () {
                        _this.deleteregister(item, '0');
                    } }
            ]
        });
        confirm.present();
    };
    //Delete User Register Function
    RequestforuserPage.prototype.deleteregister = function (item, returnc) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/deleteregister.php';
        var postData = new FormData();
        postData.append('prodid', item.progid);
        postData.append('userid', item.reg_id);
        postData.append('person_id', item.person_id);
        postData.append('return', returnc);
        postData.append('id', item.cpid);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            if (result == 'Registration Complete') {
                _this.getusercome();
                _this.gettrainers(_this.datas.progid);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    RequestforuserPage.prototype.add_person_product = function (item_person) {
        this.program = 0;
        this.selectedItem = item_person.dailyperid;
        console.log(item_person);
    };
    RequestforuserPage.prototype.add_person_private = function (item_person) {
        this.selectedItem = item_person.dailyperid;
        this.program = 1;
        this.price = 30;
    };
    RequestforuserPage.prototype.chooseclient = function (item) {
        this.selectedClient = item.reg_id;
        this.selectedClientdata = item;
        console.log(this.selectedClientdata);
        this.isenabled = true;
        if (item.costcredits == 0) {
            this.price = item.costcredits;
        }
        else {
            this.price = this.datas.costcredits;
        }
    };
    RequestforuserPage.prototype.editprogram = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__editprogram_editprogram__["a" /* EditprogramPage */], { item: this.datas });
    };
    RequestforuserPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-requestforuser',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/requestforuser/requestforuser.html"*/'<!--\nGenerated template for the RequestforuserPage page.\n\nSee http://ionicframework.com/docs/components/#navigation for more info on\nIonic pages and navigation.\n-->\n<ion-header>\n\n<ion-navbar>\n	<ion-title>Register User or Delete</ion-title>\n	<ion-buttons start>\n		<button ion-button  primary end (click)="weekcalendar()">Back to Program</button>\n		<button ion-button  primary end (click)="editprogram()">Edit Program</button>\n	</ion-buttons>\n	\n</ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n	<ion-grid class="headrow">\n		<ion-row>\n			<ion-col col-lg-4><h2 text-center>Βήμα 1 Επέλεξε Πελάτη</h2>\n			</ion-col>\n			<ion-col col-lg-4><h2 text-center>Βήμα 2 Επέλεξε Trainer</h2>\n			</ion-col>\n			<ion-col col-lg-4><h2 text-center>Πελάτες που έχουν κλείσει Ραντεβού</h2>\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n	<ion-grid>\n		<ion-row>\n			<ion-col col-lg-4>\n				<p>Διαγράψτε την αναζήτηση για να εκτελέσετε νέα ή να επαναφέρετε όλους τους πελάτες</p>\n				<ion-searchbar placeholder="Αναζήτηση με Επίθετο, Όνομα, email ή κινητό" (ionInput)="getItems($event)"></ion-searchbar>\n				<ion-list>\n					<ion-item *ngFor="let item of items" (click)="chooseclient(item)" [ngClass]="{\'active\': item.reg_id == selectedClient}">\n						<h2>{{item.fullname}}  {{item.lastname}}</h2>\n					</ion-item>\n				</ion-list>\n			</ion-col>\n			<ion-col col-lg-4>\n				<ion-list padding> \n					<ion-item>\n						<ion-thumbnail item-start>\n							<p class="dateicon">{{datas.timehour1 | date: "dd"}}</p>\n							<p class="mmicon">{{datas.timehour1 | date: "MMM"}}</p>\n						</ion-thumbnail>\n						<ion-grid>\n							<ion-row>\n								<ion-col col-lg-8><h2>{{datas.tilte}}</h2>\n									<p></p>\n									<p>Θέσεις: {{datas.availibility}}</p>\n								</ion-col>\n								<ion-col col-lg-4>\n									<p class="price">{{datas.time1}}</p>\n								</ion-col>\n								<ion-col col-12 class="divider"></ion-col>\n							</ion-row>\n						</ion-grid>\n					</ion-item>\n				</ion-list>\n\n				<div *ngIf="datas.category == \'2\'">\n					<h3 class="trainers">Chose Trainer</h3>\n					<ion-list  class="personlist"> \n						<ion-item ion-item *ngFor="let person of trainers">\n							<ion-grid>\n								<ion-row *ngIf="person.availibility === person.numclients">\n									<ion-col col-8 class="leftbox" [ngClass]="{\'active\': person.dailyperid == selectedItem && program == 0}">\n										<button (click)="add_person_product(person)" >\n											<span class="namep">{{person.person}}</span> \n											<span class="spot">Spots remain: {{person.availibility}}</span>\n										</button>\n									</ion-col>\n									<ion-col col-3 offset-1 class="rightbox" [ngClass]="{\'active\': person.dailyperid == selectedItem && program == 1}">\n										<button (click)="add_person_private(person)">Private</button>\n									</ion-col>\n								</ion-row>\n\n								<ion-row *ngIf="person.availibility != person.numclients">\n									<ion-col col-12 class="leftbox" [ngClass]="{\'active\': person.dailyperid == selectedItem  && program == 0}" *ngIf="person.availibility != 0">\n										<button (click)="add_person_product(person)"  >\n											<span class="namep">{{person.person}}</span>  \n											<span class="spot"  >Spots remain: {{person.availibility}}</span>\n										</button>\n									</ion-col>\n									<ion-col col-12 class="leftbox full" *ngIf="person.availibility == 0">\n										<button>\n											<span class="namep">{{person.person}}</span> \n											<span class="spot" >Spots remain: Full</span>\n										</button>\n									</ion-col>\n								</ion-row>\n							</ion-grid>\n						</ion-item>\n					</ion-list>\n\n				</div>\n\n\n				<div *ngIf="datas.category != \'2\'">\n					<ion-list  class="personlist"> \n						<ion-item ion-item *ngFor="let person of trainers">\n							<ion-grid>\n								<ion-row>\n									<ion-col col-12 class="leftbox" *ngIf="person.availibility != 0">\n										<button (click)="add_person_product(person)"  >\n											<span class="namep">{{person.person}}</span>\n											<span class="spot"  >Spots remain: {{person.availibility}}</span>\n										</button>\n									</ion-col>\n									<ion-col col-12 class="leftbox full" *ngIf="person.availibility == 0">\n									<button>\n										<span class="namep">{{person.person}}</span>\n										<span class="spot" >Spots remain: Full</span>\n									</button>\n									</ion-col>\n								</ion-row>\n							</ion-grid>\n						</ion-item>\n					</ion-list>  \n				</div>	\n\n				<p paddings></p>\n\n				 <ion-label color="primary" stacked>Σχόλιο</ion-label>\n          		<ion-textarea [(ngModel)]="comments"></ion-textarea> \n\n				<div *ngIf="program === 0">\n					<h4 text-center *ngIf="datas.costcredits === 0"></h4>\n					<h4 text-center *ngIf="datas.costcredits != 0">{{price}} CREDITS</h4>\n					<button ion-button primary class="register" [disabled]="!isenabled" (click)="register()" *ngIf="datas.costcredits === 0">Join Now</button>\n					<button ion-button primary class="register"  [disabled]="!isenabled" (click)="register()" *ngIf="datas.costcredits != 0">Join Now </button>\n				</div>\n\n				<div *ngIf="program === 1">\n					<h4 text-center>30 Credits</h4>\n					<button ion-button primary class="register" [disabled]="!isenabled" (click)="register()">Join Now</button>\n				</div>\n			</ion-col>\n\n			<ion-col col-lg-4>\n				<ion-list>\n					<ion-item *ngFor="let item of users" (click)="deleteprogram(item)" >\n						<h3>{{item.fullname}}  {{item.lastname}}</h3>\n						<p>{{item.tilte}}</p>\n						<p>Trainer: {{item.person}}</p> \n					</ion-item> \n				</ion-list>\n			</ion-col>\n		</ion-row>\n	</ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/requestforuser/requestforuser.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], RequestforuserPage);
    return RequestforuserPage;
}());

//# sourceMappingURL=requestforuser.js.map

/***/ }),

/***/ 141:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_storage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_interval__ = __webpack_require__(246);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_interval___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_observable_interval__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__customerview_customerview__ = __webpack_require__(69);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = /** @class */ (function () {
    function HomePage(navCtrl, http, storage, alertCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.nowdate = new Date().toISOString();
        this.daylist = 1;
        this.loadData();
        this.getbookstatus();
        this.expireuser();
        this.sub = __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].interval(300000)
            .subscribe(function (val) {
            console.log('called');
            _this.getusercome(_this.daylist);
        });
    }
    HomePage_1 = HomePage;
    HomePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://zonepage.gr/gymapp/appcrm/userentries.php');
        data.subscribe(function (result) {
            _this.items = result;
            console.log(_this.items);
        });
    };
    HomePage.prototype.getusercome = function (value) {
        var _this = this;
        console.log(value);
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/comingusers2.php';
        var postData = new FormData();
        postData.append('daylist', this.daylist);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    HomePage.prototype.onChange = function (value) {
        var _this = this;
        this.daylist = value;
        this.sub.unsubscribe();
        this.getusercome(this.daylist);
        this.sub = __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"].interval(300000)
            .subscribe(function (val) {
            console.log('called');
            _this.getusercome(_this.daylist);
        });
    };
    HomePage.prototype.update = function (item) {
        var _this = this;
        console.log(item);
        console.log(item);
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Προσοχή θα πρέπει να έχει ανεωθεί το πρόγραμμα του ' + name,
            message: '',
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'ΝΑΙ ',
                    handler: function () {
                        _this.updateprogram(item);
                    }
                }
            ]
        });
        confirm.present();
    };
    HomePage.prototype.expireuser = function () {
        var _this = this;
        var data;
        data = this.http.get('http://zonepage.gr/gymapp/appcrm/expireusers.php');
        data.subscribe(function (result) {
            _this.expireusers = result;
            console.log(_this.expireusers);
        });
    };
    HomePage.prototype.gotoprogram = function (item) {
        var _this = this;
        console.log(item);
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε να ακυρώσετε το ραντεβού για ' + name,
            message: 'Επιλέγοντα "ΝΑΙ" επιστρέφονται στον πελάτη τα μισά Credits. Επιλέγοντας ναι με επιστροφή Credits επιστρέφονται όλα τα Credits',
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'ΝΑΙ τα μισά',
                    handler: function () {
                        _this.returncredits = (parseInt(item.costcredits) / 2).toFixed(0);
                        _this.deleteregister(item, _this.returncredits);
                        console.log(_this.returncredits);
                    }
                },
                {
                    text: 'ΝΑΙ Επιστροφή Credits',
                    handler: function () {
                        _this.deleteregister(item, item.costcredits);
                    }
                },
                { text: 'Να μην Επιστραφούν',
                    handler: function () {
                        _this.deleteregister(item, '0');
                    } }
            ]
        });
        confirm.present();
    };
    HomePage.prototype.notification = function (item) {
        var _this = this;
        console.log(item);
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Do you want to send a reminder to ' + name,
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.sendreminder(item);
                    }
                }
            ]
        });
        confirm.present();
    };
    HomePage.prototype.sendreminder = function (item) {
        this.sendpushnotification(item);
        console.log(item);
        var url = 'https://mendez.gr/crm/expirepush.php';
        var postData = new FormData();
        postData.append('id', item.reg_id);
        postData.append('email', item.email);
        postData.append('description', item.description);
        postData.append('expires', item.timehour1);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    HomePage.prototype.sendpushnotification = function (item) {
        console.log(item);
        var url = 'https://zonepage.gr/gymapp/expirepushnotification.php';
        var postData = new FormData();
        postData.append('id', item.reg_id);
        postData.append('description', item.description);
        postData.append('expires', item.timehour1);
        postData.append('monthid', item.monthid);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    HomePage.prototype.gologout = function () {
        this.storage.clear();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    HomePage.prototype.deleteregister = function (item, returnc) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/deleteregister.php';
        var postData = new FormData();
        postData.append('prodid', item.progid);
        postData.append('userid', item.reg_id);
        postData.append('return', returnc);
        postData.append('person_id', item.person_id);
        postData.append('id', item.cpid);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Add reminder		 
            if (result == 'Registration Complete') {
                _this.navCtrl.setRoot(HomePage_1);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    HomePage.prototype.updateprogram = function (item) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/updateuserprogram.php';
        var postData = new FormData();
        postData.append('prodid', item.progid);
        postData.append('userid', item.reg_id);
        postData.append('id', item.monthid);
        postData.append('expire', item.expires);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Add reminder		 
            if (result > 0) {
                _this.navCtrl.setRoot(HomePage_1);
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_2.present();
            }
        });
    };
    HomePage.prototype.booking = function (event) {
        if (event.value == false) {
            this.lastbookstate = 0;
        }
        if (event.value == true) {
            this.lastbookstate = 1;
        }
        console.log(event.value);
        var url = 'https://zonepage.gr/gymapp/appcrm/bookstate.php';
        var postData = new FormData();
        postData.append('bookstate', this.lastbookstate);
        postData.append('bookfun', 'change');
        var data = this.http.post(url, postData);
        data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    HomePage.prototype.getbookstatus = function () {
        var _this = this;
        var url = 'https://zonepage.gr/gymapp/appcrm/bookstate.php';
        var postData = new FormData();
        postData.append('bookfun', 'get');
        var data = this.http.post(url, postData);
        data.subscribe(function (dataa) {
            _this.lastbookstate = dataa['state'];
            if (_this.lastbookstate == 0) {
                _this.lastbook = false;
            }
            if (_this.lastbookstate == 1) {
                _this.lastbook = true;
            }
        });
    };
    HomePage.prototype.gotoclient = function (item) {
        //console.log (item.idpro);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__customerview_customerview__["a" /* CustomerviewPage */], { item: item });
    };
    HomePage = HomePage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n  \n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Home</ion-title>\n\n	<ion-buttons end>\n<button ion-button outline end (click)="gologout()">Logout</button>\n\n</ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n<ion-grid>\n  <ion-row>\n    <ion-col col-lg-4>\n    	    <ion-list>\n<ion-item>\n    <ion-label>Ενεργοποίση Last  Minutes Booking</ion-label>\n    <ion-toggle [(ngModel)]="lastbook" (ionChange)="booking($event)"></ion-toggle>\n  </ion-item>\n</ion-list>\n    <h2>Ραντεβού</h2> \n\n     <ion-label>Filter</ion-label>\n    <ion-select [(ngModel)]="toppings" multiple="false" cancelText="Cancel" okText="Ok" (ionChange)="onChange($event)">\n      <ion-option value = "1" selected>Τελευταία Ραντεβού</ion-option>\n      <ion-option value = "2">Ραντεβού Σήμερα</ion-option>  \n    </ion-select>\n	<p>Μπορείτε να ακυρώσετε συγκεκριμένο ραντεβού με Click</p>\n	<p>Γίνεται αναναίωση κάθε 5 λεπτά</p>\n	 <ion-list>\n\n <ion-item *ngFor="let item of users" (click)="gotoprogram(item)">\n    <h3>{{item.fullname}}  {{item.lastname}}</h3> \n	<p>{{item.tilte}} - Trainer: {{item.person}}</p>\n	<p>{{item.timehour1}}</p>\n	<p>Σχόλιο : {{item.comment}}</p>\n    </ion-item> \n\n\n\n</ion-list>\n    </ion-col >\n	\n	\n	 <ion-col col-lg-4>\n    <h2>Είσοδος Πελατών στο Mendez</h2> \n	<p></p>\n	 <ion-list>\n\n <ion-item *ngFor="let item of items">\n    <h3>{{item.fullname}}  {{item.lastname}}</h3> \n	<p>{{item.tilte}}  {{item.time1}}</p>\n	<p>{{item.entrytime}}</p>\n    </ion-item> \n\n\n\n</ion-list>\n    </ion-col >\n	\n	\n	\n	\n	\n		 <ion-col col-lg-4>\n    <h2>Πακέτα πελατών που λήγουν</h2> \n	<p>Στις επόμενες 30 ημέρες. Επιλέξτε πελάτη για να του στείλετε notification</p>\n	 <ion-list>\n\n <ion-item *ngFor="let item of expireusers" class="expires" (click)="gotoclient(item)" [ngClass]="{\'expire\' : (item.expires < nowdate)}">\n    <h3>{{item.fullname}}  {{item.lastname}}</h3> \n	  <p *ngIf="item.getcredits > 0">{{ item.getcredits }} Credits from {{item.sender}}</p>\n  <p>{{item.title}}  {{item.expires}}</p>	\n    </ion-item> \n\n\n\n</ion-list>\n    </ion-col >\n \n	\n	\n\n	\n	\n  </ion-row>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], HomePage);
    return HomePage;
    var HomePage_1;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 142:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SendzoomPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the SendzoomPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var SendzoomPage = /** @class */ (function () {
    function SendzoomPage(navCtrl, navParams, viewCtrl, alertCtrl, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        console.log('UserId', navParams.get('userId'));
        this.useremail = navParams.get('userId');
        this.userid = navParams.get('reg_id');
        this.link = navParams.get('link');
    }
    SendzoomPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SendzoomPage');
        console.log(document.getElementById('zoomurl').textContent);
    };
    SendzoomPage.prototype.dismissModal = function () {
        var data = { 'foo': 'bar' };
        this.viewCtrl.dismiss(data);
    };
    SendzoomPage.prototype.sendmesage = function () {
        var _this = this;
        if (this.useremail) {
            var url = 'https://zonepage.gr/gymapp/appcrm/senduserzoom.php';
            var postData = new FormData();
            postData.append('id', this.userid);
            postData.append('description', this.messagetext);
            postData.append('email', this.useremail);
            this.data = this.http.post(url, postData);
            this.data.subscribe(function (dataa) {
                console.log(dataa);
                if (dataa == "success") {
                    var alert_1 = _this.alertCtrl.create({
                        title: "Στάλθηκε με επιτυχία",
                        //subTitle: result,
                        buttons: ['OK']
                    });
                    alert_1.present();
                    _this.messagetext = '';
                    _this.nottitle = '';
                    _this.dismissModal();
                }
                else {
                }
            });
        }
        else {
            var alert_2 = this.alertCtrl.create({
                title: "Συμπληρώστε όλα τα πεδία",
                //subTitle: result,
                buttons: ['OK']
            });
            alert_2.present();
        }
    };
    SendzoomPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-sendzoom',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/sendzoom/sendzoom.html"*/'<!--\n  Generated template for the SendzoomPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n\n\n\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Send Zoom Request</ion-title>\n      	<ion-buttons end>\n<button ion-button end (click)="dismissModal()">Close</button>\n\n</ion-buttons>\n  </ion-navbar>\n\n\n</ion-header>\n<ion-content padding> \n<h2> Αποστολή invitation Zoom στον {{useremail}}</h2>\n\n<p id="zoomurl"></p>\n<ion-input  class="intitle" id="urllink" placeholder="Paste Zoom Url" [(ngModel)]="messagetext"></ion-input>\n                            <button ion-button (click)="sendmesage()">Send Message</button>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/sendzoom/sendzoom.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], SendzoomPage);
    return SendzoomPage;
}());

//# sourceMappingURL=sendzoom.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddclassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_images_images__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddclassPage = /** @class */ (function () {
    function AddclassPage(navCtrl, navParams, _IMAGES, _ALERT, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this._IMAGES = _IMAGES;
        this._ALERT = _ALERT;
        this.http = http;
        /**
         * @name isSelected
         * @type Boolean
         * @public
         * @description              Used to switch DOM elements on/off depending on whether an image has been selected
         */
        this.isSelected = true;
    }
    AddclassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddclassPage');
    };
    AddclassPage.prototype.selectFileToUpload = function (event) {
        var _this = this;
        this._IMAGES
            .handleImageSelection(event)
            .subscribe(function (res) {
            // Retrieve the file type from the base64 data URI string
            _this._SUFFIX = res.split(':')[1].split('/')[1].split(";")[0];
            // Do we have correct file type?
            if (_this._IMAGES.isCorrectFileType(_this._SUFFIX)) {
                // Hide the file input field, display the image in the component template
                // and display an upload button
                _this.isSelected = true;
                _this.image = res;
            }
            else {
                _this.displayAlert('You need to select an image file with one of the following types: jpg, gif or png');
            }
        }, function (error) {
            console.dir(error);
            _this.displayAlert(error.message);
        });
    };
    /**
     * @public
     * @method uploadFile
     * @description    			Handles uploading the selected image to the remote PHP script
     * @return {none}
     */
    AddclassPage.prototype.uploadFile = function () {
        var _this = this;
        this._IMAGES
            .uploadImageSelection(this.image, this._SUFFIX)
            .subscribe(function (res) {
            _this.displayAlert(res.message);
        }, function (error) {
            console.dir(error);
            _this.displayAlert(error.message);
        });
    };
    /**
     * @public
     * @method displayAlert
     * @param message  {string}  The message to be displayed to the user
     * @description    			Use the Ionic AlertController API to provide user feedback
     * @return {none}
     */
    AddclassPage.prototype.displayAlert = function (message) {
        var alert = this._ALERT.create({
            title: 'Heads up!',
            subTitle: message,
            buttons: ['Got it']
        });
        alert.present();
    };
    AddclassPage.prototype.gologin = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/addclass.php';
        var postData = new FormData();
        postData.append('title', this.title);
        postData.append('description', this.description);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
            console.log(data);
        });
    };
    AddclassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-addclass',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/addclass/addclass.html"*/'<!--\n  Generated template for the AddclassPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Προσθήκη Class</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-grid class="headrow">\n  <ion-row>\n    <ion-col col-lg-12><h3>Στοιχεία</h3>\n	\n	\n	<ion-list>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>Τίτλος</ion-label>\n    <ion-input type="text" placeholder="Title" [(ngModel)]="title"></ion-input>\n  </ion-item>\n   <ion-item>\n    <ion-label color="primary" stacked>Περιγραφή</ion-label>\n    <ion-input type="textarea" placeholder="" [(ngModel)]="description"></ion-input>\n  </ion-item>\n    \n     <ion-item>\n    <ion-label color="primary" stacked>Photo Cover</ion-label>\n\n      <h2>Select image and upload</h2>\n      <ion-input  type="file" accept="image/*" (change)="selectFileToUpload($event)"></ion-input>\n  \n   \n\n         <!-- Display the selected image -->\n\n  \n  </ion-item>\n  \n	</ion-list>\n	\n	</ion-col>\n  </ion-row>\n</ion-grid>\n\n<button ion-button block outline (click)="gologin()">Προσθήκη</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/addclass/addclass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_images_images__["a" /* ImagesProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], AddclassPage);
    return AddclassPage;
}());

//# sourceMappingURL=addclass.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddcustomerPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__customers_customers__ = __webpack_require__(68);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the AddcustomerPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddcustomerPage = /** @class */ (function () {
    function AddcustomerPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
    }
    AddcustomerPage.prototype.gologin = function () {
        var _this = this;
        var url = 'https://zonepage.gr/gymapp/appcrm/addcustomer.php';
        var postData = new FormData();
        if (this.small == true) {
            this.credits = "110";
        }
        else if (this.medium == true) {
            this.credits = "220";
        }
        else if (this.large == true) {
            this.credits = "440";
        }
        else {
            this.credits = "0";
        }
        if (this.personalmonthly == true) {
            this.semipersonal = "4";
        }
        else if (this.personalthree == true) {
            this.semipersonal = "5";
        }
        else if (this.personalyear == true) {
            this.semipersonal = "6";
        }
        else {
            this.semipersonal = "0";
        }
        if (this.pilatesmonthly == true) {
            this.pilates = "7";
        }
        else if (this.pilatesthree == true) {
            this.pilates = "8";
        }
        else if (this.pilatesyear == true) {
            this.pilates = "9";
        }
        else {
            this.pilates = "0";
        }
        if (this.yogamonthly == true) {
            this.yoga = "1";
        }
        else if (this.yogathree == true) {
            this.yoga = "2";
        }
        else if (this.yogayear == true) {
            this.yoga = "3";
        }
        else {
            this.yoga = "0";
        }
        postData.append('firstname', this.firstname);
        postData.append('lastname', this.lastname);
        postData.append('adresss', this.adress);
        postData.append('phone', this.phone);
        postData.append('mobilephone', this.mobilephone);
        postData.append('datebirth', this.birthdate);
        postData.append('email', this.email);
        postData.append('credits', this.credits);
        postData.append('semipersonal', this.semipersonal);
        postData.append('pilates', this.pilates);
        postData.append('yoga', this.yoga);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
            _this.sendmail(data);
            console.log(data);
        });
        //console.log (this.yoga);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__customers_customers__["a" /* CustomersPage */]);
    };
    AddcustomerPage.prototype.sendmail = function (pass) {
        var url = 'https://mendez.gr/crm/newuseremail.php';
        var postData = new FormData();
        postData.append('name', this.firstname);
        postData.append('email', this.email);
        postData.append('pass', pass);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    AddcustomerPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddcustomerPage');
    };
    AddcustomerPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-addcustomer',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/addcustomer/addcustomer.html"*/'<!--\n  Generated template for the AddcustomerPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Προσθήκη Νέου Πελάτη</ion-title>\n	<button ion-button block outline (click)="gologin()">Εγγραφή</button>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n<ion-grid class="headrow">\n  <ion-row>\n    <ion-col col-lg-3><h3>Προσωπικά Στοιχεία</h3>\n	\n	\n	<ion-list>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>\'Ονομα</ion-label>\n    <ion-input type="text" placeholder="Όνομα" [(ngModel)]="firstname"></ion-input>\n  </ion-item>\n   <ion-item>\n    <ion-label color="primary" stacked>Επίθετο</ion-label>\n    <ion-input type="text" placeholder="Επίθετο" [(ngModel)]="lastname"></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label color="primary" stacked>Ημερομηνία Γέννησης</ion-label>\n     <ion-datetime displayFormat="MM/DD/YYYY" [(ngModel)]="birthdate"></ion-datetime>\n  </ion-item>\n  \n   <ion-item>\n    <ion-label color="primary" stacked>Σταθερο Τηλέφωνο</ion-label>\n    <ion-input type="number" placeholder="00000" [(ngModel)]="phone"></ion-input>\n  </ion-item>\n  \n  \n     <ion-item>\n    <ion-label color="primary" stacked>Κινητό Τηλέφωνο</ion-label>\n    <ion-input type="number" placeholder="00000" [(ngModel)]="mobilephone"></ion-input>\n  </ion-item>\n  \n    <ion-item>\n    <ion-label color="primary" stacked>Email</ion-label>\n    <ion-input type="email" placeholder="email@emeil.com" [(ngModel)]="email"></ion-input>\n  </ion-item>\n  \n     <ion-item>\n    <ion-label color="primary" stacked>Διεύθυνση Κατοικίας</ion-label>\n    <ion-input type="Text" placeholder="Κατοικία" [(ngModel)]="adress"></ion-input>\n  </ion-item>\n  \n	</ion-list>\n	\n	  </ion-col>\n    <ion-col col-lg-3><h3>Ιατρικά Στοιχεία</h3>\n	<ion-list>\n	\n	<ion-item>\n  <ion-label>Gender</ion-label>\n  <ion-select [(ngModel)]="gender" >\n    <ion-option value="f">Γυναίκα</ion-option>\n    <ion-option value="m">Άντρας</ion-option>\n  </ion-select>\n</ion-item>\n\n <ion-item>\n    <ion-label color="primary" stacked >Ύψος (m)</ion-label>\n    <ion-input type="number" placeholder="m" [(ngModel)]="height"></ion-input>\n  </ion-item>\n  \n   <ion-item>\n    <ion-label color="primary" stacked>Βάρος)</ion-label>\n    <ion-input type="number" placeholder="Kg" [(ngModel)]="weight"></ion-input>\n  </ion-item>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>Παθήσεις</ion-label>\n    <ion-textarea></ion-textarea>\n  </ion-item>\n	\n	</ion-list>\n	\n	\n	\n	\n	  </ion-col>\n\n    <ion-col col-lg-3><h2>Επιλογή Credits</h2>\n	<ion-list>\n\n  <ion-item>\n    <ion-label>Small<p>100 Credits + 10 Δώρο</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="small" checked="false"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n    <ion-label>Medium<p>200 Credits + 20 Δώρο</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="medium"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n    <ion-label>Large<p>400 Credits + 40 Δώρο</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="large"></ion-checkbox>\n  </ion-item>\n\n</ion-list>\n</ion-col>\n	<ion-col col-lg-3><h2>Επιλογή Προγράμματος</h2>\n	\n	\n	\n	<h3>Semi Personal</h3>\n		<ion-list>\n\n  <ion-item>\n    <ion-label>Μηνιαίο Πακέτο<p>Διάρκεια 1 μήνας</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="personalmonthly"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n     <ion-label>3 μηνο Πακέτο<p>Διάρκεια 3 μήνες</p> </ion-label>\n	\n	\n    <ion-checkbox [(ngModel)]="personalthree"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n      <ion-label>Ετήσιο Πακέτο<p>Διάρκεια 12 μήνες</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="personalyear"></ion-checkbox>\n  </ion-item>\n\n</ion-list>\n\n<h3>Pilates</h3>\n		<ion-list>\n\n  <ion-item>\n    <ion-label>Μηνιαίο Πακέτο<p>Διάρκεια 1 μήνας</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="pilatesmonthly"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n     <ion-label>3 μηνο Πακέτο<p>Διάρκεια 3 μήνες</p> </ion-label>\n	\n	\n    <ion-checkbox [(ngModel)]="pilatesthree"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n      <ion-label>Ετήσιο Πακέτο<p>Διάρκεια 12 μήνες</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="pilatesyear"></ion-checkbox>\n  </ion-item>\n\n</ion-list>\n\n<h3>Yoga</h3>\n		<ion-list>\n\n  <ion-item>\n    <ion-label>Μηνιαίο Πακέτο<p>Διάρκεια 1 μήνας</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="yogamonthly"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n     <ion-label>3 μηνο Πακέτο<p>Διάρκεια 3 μήνες</p> </ion-label>\n	\n	\n    <ion-checkbox [(ngModel)]="yogathree"></ion-checkbox>\n  </ion-item>\n\n  <ion-item>\n      <ion-label>Ετήσιο Πακέτο<p>Διάρκεια 12 μήνες</p> </ion-label>\n	\n    <ion-checkbox [(ngModel)]="yogayear"></ion-checkbox>\n  </ion-item>\n\n</ion-list>\n	\n	</ion-col>\n\n  </ion-row>\n</ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/addcustomer/addcustomer.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], AddcustomerPage);
    return AddcustomerPage;
}());

//# sourceMappingURL=addcustomer.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UsermetricsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__useraddmetric_useraddmetric__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__viewmetrics_viewmetrics__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the UsermetricsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UsermetricsPage = /** @class */ (function () {
    function UsermetricsPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.datas = this.navParams.get('item');
        this.getmetrics();
    }
    UsermetricsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UsermetricsPage');
    };
    UsermetricsPage.prototype.getmetrics = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/getmetrics.php';
        var postData = new FormData();
        postData.append('id', this.datas);
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items = data;
            console.log(data);
            //this.storage.set('id', data);		 
            //console.log (this.items[category]);
        });
    };
    UsermetricsPage.prototype.addmetrics = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__useraddmetric_useraddmetric__["a" /* UseraddmetricPage */], { item: this.datas });
    };
    UsermetricsPage.prototype.gotoproduct = function (item) {
        //console.log (item.idpro);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__viewmetrics_viewmetrics__["a" /* ViewmetricsPage */], { item: item });
    };
    UsermetricsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-usermetrics',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/usermetrics/usermetrics.html"*/'<!--\n  Generated template for the UsermetricsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>usermetrics</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<button ion-button outline end (click)="addmetrics()">Add New Metric</button>\n\n<ion-list>\n    <ion-item *ngFor="let item of items" (click)="gotoproduct(item)">\n	<ion-grid>\n  <ion-row>\n	<ion-col col-2>\n      <h2>#{{item.id}}</h2>\n	 \n   </ion-col>\n	\n    <ion-col col-6><h2>{{item.created}}</h2>\n	 \n  \n	</ion-col>\n  </ion-row>\n</ion-grid>\n	\n	</ion-item>\n	</ion-list>\n	\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/usermetrics/usermetrics.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], UsermetricsPage);
    return UsermetricsPage;
}());

//# sourceMappingURL=usermetrics.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UseraddmetricPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the UseraddmetricPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var UseraddmetricPage = /** @class */ (function () {
    function UseraddmetricPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.datas = this.navParams.get('item');
    }
    UseraddmetricPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad UseraddmetricPage');
    };
    UseraddmetricPage.prototype.addmetric = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/addmetric.php';
        var postData = new FormData();
        postData.append('id', this.datas);
        postData.append('weight', this.weight);
        postData.append('fat', this.fat);
        postData.append('water', this.water);
        postData.append('muscle', this.muscle);
        postData.append('ostiki', this.ostiki);
        postData.append('age', this.age);
        postData.append('splaxniko', this.splaxniko);
        this.dats = this.http.post(url, postData);
        this.dats.subscribe(function (data) {
            if (data == "error") {
            }
            console.log(data);
        });
    };
    UseraddmetricPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-useraddmetric',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/useraddmetric/useraddmetric.html"*/'<!--\n  Generated template for the UseraddmetricPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Προσθήκη Μέτρησης</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>Βάρος</ion-label>\n    <ion-input type="text" placeholder="Kg" [(ngModel)]="weight"></ion-input>\n  </ion-item>\n   <ion-item>\n    <ion-label color="primary" stacked>Σωματικά Υγρα</ion-label>\n    <ion-input type="text" placeholder="%" [(ngModel)]="water"></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label color="primary" stacked>Λίπος</ion-label>\n      <ion-input type="text" placeholder="%" [(ngModel)]="fat"></ion-input>\n  </ion-item>\n  \n   <ion-item>\n    <ion-label color="primary" stacked>Μυϊκή Μάζα</ion-label>\n    <ion-input type="text" placeholder="Kg" [(ngModel)]="muscle"></ion-input>\n  </ion-item>\n  \n  \n     <ion-item>\n    <ion-label color="primary" stacked>Οστική Μάζα</ion-label>\n    <ion-input type="text" placeholder="v" [(ngModel)]="ostiki"></ion-input>\n  </ion-item>\n  \n    <ion-item>\n    <ion-label color="primary" stacked>Μεταβολική Ηλικία</ion-label>\n    <ion-input type="text" placeholder="Age" [(ngModel)]="age"></ion-input>\n  </ion-item>\n  \n     <ion-item>\n   <ion-label color="primary" stacked>Σπλαχνικό Λίπος</ion-label>\n    <ion-input type="text" placeholder="Kg" [(ngModel)]="splaxniko"></ion-input>\n  </ion-item>\n  \n	</ion-list>\n	<button ion-button block outline (click)="addmetric()">Προσθήκη</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/useraddmetric/useraddmetric.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], UseraddmetricPage);
    return UseraddmetricPage;
}());

//# sourceMappingURL=useraddmetric.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewmetricsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ViewmetricsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewmetricsPage = /** @class */ (function () {
    function ViewmetricsPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.datas = this.navParams.get('item');
        this.weight = this.datas.weight;
        this.fat = this.datas.fat;
        this.water = this.datas.water;
        this.muscle = this.datas.muscle;
        this.ostiki = this.datas.ostiki;
        this.age = this.datas.age;
        this.splaxniko = this.datas.splaxniko;
    }
    ViewmetricsPage.prototype.addmetric = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/updatemetric.php';
        var postData = new FormData();
        postData.append('id', this.datas.id);
        postData.append('weight', this.weight);
        postData.append('fat', this.fat);
        postData.append('water', this.water);
        postData.append('muscle', this.muscle);
        postData.append('ostiki', this.ostiki);
        postData.append('age', this.age);
        postData.append('splaxniko', this.splaxniko);
        this.dats = this.http.post(url, postData);
        this.dats.subscribe(function (data) {
            if (data == "error") {
            }
            console.log(data);
        });
    };
    ViewmetricsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ViewmetricsPage');
    };
    ViewmetricsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-viewmetrics',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/viewmetrics/viewmetrics.html"*/'<!--\n  Generated template for the ViewmetricsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>View/Update User Metrics</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-list>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>Βάρος</ion-label>\n    <ion-input type="text" placeholder="" [(ngModel)]="weight"></ion-input>\n  </ion-item>\n   <ion-item>\n    <ion-label color="primary" stacked>Σωματικά Υγρα</ion-label>\n    <ion-input type="text" placeholder="%" [(ngModel)]="water"></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label color="primary" stacked>Λίπος</ion-label>\n      <ion-input type="text" placeholder="%" [(ngModel)]="fat"></ion-input>\n  </ion-item>\n  \n   <ion-item>\n    <ion-label color="primary" stacked>Μυϊκή Μάζα</ion-label>\n    <ion-input type="text" placeholder="Kg" [(ngModel)]="muscle"></ion-input>\n  </ion-item>\n  \n  \n     <ion-item>\n    <ion-label color="primary" stacked>Οστική Μάζα</ion-label>\n    <ion-input type="text" placeholder="v" [(ngModel)]="ostiki"></ion-input>\n  </ion-item>\n  \n    <ion-item>\n    <ion-label color="primary" stacked>Μεταβολική Ηλικία</ion-label>\n    <ion-input type="text" placeholder="Age" [(ngModel)]="age"></ion-input>\n  </ion-item>\n  \n     <ion-item>\n   <ion-label color="primary" stacked>Σπλαχνικό Λίπος</ion-label>\n    <ion-input type="text" placeholder="Kg" [(ngModel)]="splaxniko"></ion-input>\n  </ion-item>\n  \n	</ion-list>\n	<button ion-button block outline (click)="addmetric()">Update</button>\n\n</ion-content>'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/viewmetrics/viewmetrics.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], ViewmetricsPage);
    return ViewmetricsPage;
}());

//# sourceMappingURL=viewmetrics.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TargetsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the TargetsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TargetsPage = /** @class */ (function () {
    function TargetsPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.datas = this.navParams.get('item');
        this.getmetrics();
        this.get_lastmetrics();
    }
    TargetsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TargetsPage');
    };
    TargetsPage.prototype.getmetrics = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/gettargets.php';
        var postData = new FormData();
        postData.append('id', this.datas);
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items = data;
            console.log(data);
            //this.storage.set('id', data);		 
            //console.log (this.items[category]);
        });
    };
    TargetsPage.prototype.get_lastmetrics = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/target_metrics.php';
        var postData = new FormData();
        postData.append('id', this.datas);
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items_metr = data;
            console.log(_this.items_metr);
            //this.storage.set('id', data);		 
            //console.log (this.items[category]);
        });
    };
    TargetsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-targets',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/targets/targets.html"*/'<!--\n  Generated template for the TargetsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Στόχοι Πελάτη</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n\n    <ion-list>\n        <ion-item *ngFor="let item of items" (click)="gotoproduct(item)">\n            <ion-grid>\n                <ion-row>\n                    <ion-col col-2>\n                        <h2>#{{item.target_id}}</h2>\n                    </ion-col>\n                    <ion-col col-6>\n                        <h2>{{item.created}}</h2>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n\n        </ion-item>\n    </ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/targets/targets.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], TargetsPage);
    return TargetsPage;
}());

//# sourceMappingURL=targets.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditcustomerprogramPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the EditcustomerprogramPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditcustomerprogramPage = /** @class */ (function () {
    function EditcustomerprogramPage(navCtrl, navParams, http, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.datas = this.navParams.get('item');
        this.person = this.navParams.get('person');
        console.log(this.datas);
        var dateString = this.datas.create;
        this.startdate = new Date(dateString).toISOString();
        this.expire = this.datas.expires;
    }
    EditcustomerprogramPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditcustomerprogramPage');
    };
    EditcustomerprogramPage.prototype.sendreminder = function () {
        var url = 'https://mendez.gr/crm/expirepush.php';
        var postData = new FormData();
        postData.append('id', this.person.reg_id);
        postData.append('email', this.person.email);
        postData.append('description', this.datas.description);
        postData.append('expires', this.datas.expires);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    EditcustomerprogramPage.prototype.sendpushnotification = function () {
        var url = 'https://zonepage.gr/gymapp/expirepushnotification.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        postData.append('description', this.datas.description);
        postData.append('expires', this.datas.timehour1);
        postData.append('monthid', this.datas.monthid);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    EditcustomerprogramPage.prototype.noupdate = function () {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/updateuserprogram.php';
        var postData = new FormData();
        postData.append('id', this.datas.reqid);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Add reminder		 
            if (result > 0) {
                var alert_1 = _this.alertCtrl.create({
                    title: "Succesfull",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_1.present();
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_2.present();
            }
        });
    };
    EditcustomerprogramPage.prototype.updatedates = function () {
        var _this = this;
        var url = 'https://zonepage.gr/gymapp/appcrm/updatedates.php';
        var postData = new FormData();
        console.log(this.expire);
        postData.append('programid', this.datas.reqid);
        postData.append('startdate', this.startdate);
        postData.append('expires', this.expire);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa > 0) {
                var alert_3 = _this.alertCtrl.create({
                    title: "Succesfull",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_3.present();
            }
            else {
                var alert_4 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_4.present();
            }
        });
    };
    EditcustomerprogramPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-editcustomerprogram',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/editcustomerprogram/editcustomerprogram.html"*/'<!--\n  Generated template for the EditcustomerprogramPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>editcustomerprogram</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<h2>{{datas.title}}</h2>\n <ion-list>\n        <ion-item>\n          <ion-label color="primary" stacked>Ημερομηνία Ενεργοποίησης</ion-label>\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="startdate" max="2030-08-23"></ion-datetime>\n        </ion-item>\n         <ion-item>\n          <ion-label color="primary" stacked>Ημερομηνία Λήξης</ion-label>\n          <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="expire" max="2030-08-23"></ion-datetime>\n        </ion-item>\n        </ion-list>	\n         <button ion-button (click)="updatedates()">Update  Ημερομηνίες</button>\n         <button ion-button (click)="sendreminder()">Αποστολή Email Λήξης</button>\n         <button ion-button (click)="noupdate()">Δεν θα Αναναιωθεί</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/editcustomerprogram/editcustomerprogram.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], EditcustomerprogramPage);
    return EditcustomerprogramPage;
}());

//# sourceMappingURL=editcustomerprogram.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditprogramPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__ = __webpack_require__(47);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the EditprogramPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditprogramPage = /** @class */ (function () {
    function EditprogramPage(navCtrl, navParams, http, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.datas = this.navParams.get('item');
        this.getData();
        this.getusercome();
        console.log(this.datas);
    }
    EditprogramPage.prototype.getData = function () {
        var _this = this;
        var data;
        this.datass = this.http.get('https://zonepage.gr/gymapp/appcrm/getclasses.php');
        this.datass.subscribe(function (result) {
            _this.items = result;
        });
        this.loadData();
    };
    EditprogramPage.prototype.loadData = function () {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/updaterogram.php';
        var postData = new FormData();
        postData.append('progid', this.datas.progid);
        postData.append('todo', 'get');
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.userdata = result[0];
            _this.programtitle = _this.userdata.tilte;
            _this.description = _this.userdata.description;
            _this.numclients = _this.userdata.persons[0].numclients;
            _this.credits = _this.userdata.costcredits;
            _this.category2 = _this.userdata.category;
            _this.person21 = _this.userdata.persons[0].personids;
            _this.person21id = _this.userdata.persons[0].dailyperid;
            if (_this.userdata.persons[1]) {
                _this.person22 = _this.userdata.persons[1].personids;
                _this.person22id = _this.userdata.persons[1].dailyperid;
            }
            else {
                _this.person22 = '0';
                _this.person23id = '0';
            }
            if (_this.userdata.persons[2]) {
                _this.person23 = _this.userdata.persons[2].personids;
                _this.person23id = _this.userdata.persons[2].dailyperid;
            }
            else {
                _this.person23 = '0';
                _this.person23id = '0';
            }
            _this.time = _this.datas.timehour1;
            //this.storage.set('id', data);		 
            console.log(_this.userdata);
        });
    };
    EditprogramPage.prototype.updateprogram = function () {
        var _this = this;
        var url = 'https://zonepage.gr/gymapp/appcrm/updaterogram.php';
        var postData = new FormData();
        postData.append('progid', this.datas.progid);
        postData.append('todo', 'update');
        postData.append('numclients', this.numclients);
        postData.append('credits', this.credits);
        postData.append('category', this.category2);
        postData.append('person1', this.person21);
        postData.append('person2', this.person22);
        postData.append('person3', this.person23);
        postData.append('person1id', this.person21id);
        postData.append('person2id', this.person22id);
        postData.append('person3id', this.person23id);
        postData.append('timehour', this.time);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            console.log(data);
            var alert = _this.alertCtrl.create({
                title: "Η Αλλαγή Ολοκληρώθηκε",
                //subTitle: result,
                buttons: ['OK']
            });
            alert.present();
            if (data == "error") {
            }
        });
    };
    EditprogramPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditprogramPage');
    };
    EditprogramPage.prototype.getusercome = function () {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('program', this.datas.progid);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/usersinprogram2.php', postData);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    EditprogramPage.prototype.sendmesage = function () {
        var _this = this;
        if (this.messagetext && this.nottitle) {
            var url = 'https://zonepage.gr/gymapp/appcrm/sendnotigication.php';
            this.users.forEach(function (item) {
                console.log(item);
                var postData = new FormData();
                postData.append('id', item.reg_id);
                postData.append('description', _this.messagetext);
                postData.append('title', _this.nottitle);
                _this.data = _this.http.post(url, postData);
                _this.data.subscribe(function (dataa) {
                    console.log(dataa);
                    if (dataa == "success") {
                    }
                    else {
                    }
                });
            });
        }
        else {
            var alert_1 = this.alertCtrl.create({
                title: "Συμπληρώστε όλα τα πεδία",
                //subTitle: result,
                buttons: ['OK']
            });
            alert_1.present();
        }
        this.messagetext = '';
        this.nottitle = '';
    };
    EditprogramPage.prototype.deleteit = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε σίγουρα να διαγράψετε το Πρόγραμμα',
            buttons: [
                { text: 'OXI',
                    handler: function () { } },
                { text: 'ΝΑΙ',
                    handler: function () {
                        _this.deletecurrent();
                    } }
            ]
        });
        confirm.present();
    };
    EditprogramPage.prototype.deletecurrent = function () {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/deleteprod.php';
        var postData = new FormData();
        postData.append('productid', this.datas.progid);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Alert 	
            var alert = _this.alertCtrl.create({
                title: _this.datas.tilte,
                subTitle: result,
                buttons: ['OK']
            });
            alert.present();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */]);
            //Add reminder		 
            if (result == 'Complete') { }
        });
    };
    EditprogramPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-editprogram',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/editprogram/editprogram.html"*/'<!--\n  Generated template for the EditprogramPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Edit Program</ion-title>\n      <ion-buttons end>\n    <button ion-button  primary end (click)="deleteit()">Delete Current</button>\n  </ion-buttons>\n	\n  </ion-navbar>\n\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-list>\n  \n\n\n\n  <ion-item>\n    <ion-label color="primary">Αριθμός Ατόμων ανά γυμναστη</ion-label>\n    <ion-input type="number" placeholder="Number Input with no label" [(ngModel)]="numclients"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="primary">Κόστος Credits</ion-label>\n    <ion-input type="number" placeholder="Credits" [(ngModel)]="credits"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="primary">Επιλογή Κατηγορίας</ion-label>\n  <ion-select [(ngModel)]="category2">\n    <ion-option *ngFor="let field of items" value = "{{field.id}}">{{field.title}}</ion-option>\n    </ion-select>\n\n  </ion-item>\n\n  <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή 1</ion-label>\n  <ion-select [(ngModel)]="person21">\n    <ion-option value="0">Κανένας</ion-option>\n    <ion-option value="1">Γιάννης</ion-option>\n    <ion-option value="2">Γιώργος</ion-option>\n    <ion-option value="3">Νίκος</ion-option>\n    <ion-option value="4">Έλενα</ion-option>\n    <ion-option value="5">Ξένια</ion-option>\n    <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n   <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή 2</ion-label>\n  <ion-select [(ngModel)]="person22">\n    <ion-option value="0">Κανένας</ion-option>\n    <ion-option value="1">Γιάννης</ion-option>\n    <ion-option value="2">Γιώργος</ion-option>\n    <ion-option value="3">Νίκος</ion-option>\n    <ion-option value="4">Έλενα</ion-option>\n    <ion-option value="5">Ξένια</ion-option>\n      <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n   <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή 3</ion-label>\n  <ion-select [(ngModel)]="person23">\n    <ion-option value="0">Κανένας</ion-option>\n    <ion-option value="1">Γιάννης</ion-option>\n    <ion-option value="2">Γιώργος</ion-option>\n    <ion-option value="3">Νίκος</ion-option>\n    <ion-option value="4">Έλενα</ion-option>\n    <ion-option value="5">Ξένια</ion-option>\n      <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n <ion-item>\n  <ion-label>Επιλογή Ώρας</ion-label>\n  <ion-datetime displayFormat="DD/MM/YYYY HH:mm" [(ngModel)]="time" ></ion-datetime>\n</ion-item>\n\n \n \n\n  \n</ion-list>\n\n\n\n\n\n<button ion-button (click)="updateprogram()">Update</button>\n\n  <ion-grid>\n                <ion-row>\n                    <ion-col col-lg-6>\n\n<p>ΠΡΟΣΟΧΗ στο πρόγραμμα υπάρχουν πελάτες που έχουν κλείσει με συγκεκριμένο γυμναστή. Εάν αντικαταστήσετε το όνομα του Γυμναστή τότε θα μεταφερθούν αυτόματα. Εάν θέλετε να καταργήσετε εντελώς έναν γυμναστή απλά αντικαταστήστε το όνομά του με του Ενεργού</p>\n\n <ion-list>\n          <ion-item *ngFor="let item of users" (click)="deleteprogram(item)" >\n            <h3>{{item.fullname}}  {{item.lastname}}</h3>\n            <p>{{item.tilte}}</p>\n            <p>Trainer: {{item.person}}</p> \n          </ion-item> \n        </ion-list>\n</ion-col>\n\n <ion-col col-lg-6>\n\n            <h3 class="dividertitle">Αποστολή Notifications</h3>\n                            <ion-input class="intitle" type="text" placeholder="Title" value="Αλλαγή Προγράμματος"[(ngModel)]="nottitle"></ion-input>\n                            <ion-textarea  class="intitle" rows="10" placeholder="Message" [(ngModel)]="messagetext"></ion-textarea>\n                            <button ion-button (click)="sendmesage()">Send Message</button>\n</ion-col>\n\n</ion-row>\n</ion-grid>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/editprogram/editprogram.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], EditprogramPage);
    return EditprogramPage;
}());

//# sourceMappingURL=editprogram.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ClassesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__addclass_addclass__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__editclass_editclass__ = __webpack_require__(175);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the ClassesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ClassesPage = /** @class */ (function () {
    function ClassesPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadData();
    }
    ClassesPage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://zonepage.gr/gymapp/appcrm/getclasses.php');
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    ClassesPage.prototype.addclass = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__addclass_addclass__["a" /* AddclassPage */]);
    };
    ClassesPage.prototype.gotoproduct = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__editclass_editclass__["a" /* EditclassPage */], { item: item });
    };
    ClassesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ClassesPage');
    };
    ClassesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-classes',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/classes/classes.html"*/'<!--\n  Generated template for the ClassesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Classes</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-grid>\n<ion-row>\n<ion-col col-2>\n\n<button ion-button block (click)="addclass()">Προσθηκη Class</button>\n	  </ion-col>\n	  <ion-col col-10>\n\n<ion-list>\n    <ion-item *ngFor="let item of items" (click)="gotoproduct(item)">\n    <ion-grid>\n  <ion-row>\n	\n   <ion-col col-2>{{item.reg_id}}</ion-col>\n    <ion-col col-2><h2>{{item.title}}</h2>\n	  </ion-col>\n\n    <ion-col col-2>{{item.description}}</ion-col>\n	</ion-row>\n</ion-grid>\n	</ion-item>\n	</ion-list>\n\n	</ion-col>\n</ion-row>\n</ion-grid>\n      \n   \n</ion-content>\n\n\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/classes/classes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], ClassesPage);
    return ClassesPage;
}());

//# sourceMappingURL=classes.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditclassPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the EditclassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditclassPage = /** @class */ (function () {
    function EditclassPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.datas = this.navParams.get('item');
        console.log(this.datas);
        this.title = this.datas.title;
        this.description = this.datas.description;
        //this.imgurl = this.datas.topphoto;
    }
    EditclassPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad EditclassPage');
    };
    EditclassPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-editclass',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/editclass/editclass.html"*/'<!--\n  Generated template for the EditclassPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Editclass</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-grid class="headrow">\n  <ion-row>\n    <ion-col col-lg-12><h3>Στοιχεία</h3>\n	\n	\n	<ion-list>\n		 <ion-item>\n    <img [src]="imgurl">\n  </ion-item>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>Τίτλος</ion-label>\n    <ion-input type="text" placeholder="Title" [(ngModel)]="title"></ion-input>\n  </ion-item>\n   <ion-item>\n    <ion-label color="primary" stacked>Περιγραφή</ion-label>\n    <ion-input type="textarea" placeholder="" [(ngModel)]="description"></ion-input>\n  </ion-item>\n    \n     <ion-item>\n    <ion-label color="primary" stacked>Photo Cover</ion-label>\n\n      <h2>Select image and upload</h2>\n      <ion-input  type="file" accept="image/*" (change)="selectFileToUpload($event)"></ion-input>\n  \n   \n\n         <!-- Display the selected image -->\n\n  \n  </ion-item>\n  \n	</ion-list>\n	\n	</ion-col>\n  </ion-row>\n</ion-grid>\n\n<button ion-button block outline (click)="gologin()">Προσθήκη</button>\n\n</ion-content>'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/editclass/editclass.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], EditclassPage);
    return EditclassPage;
}());

//# sourceMappingURL=editclass.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CreateinvoicePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the CreateinvoicePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CreateinvoicePage = /** @class */ (function () {
    function CreateinvoicePage(navCtrl, navParams, http, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.date = new Date().toISOString();
        this.id = parseInt(this.navParams.get('item'));
        this.id = this.id + 1;
        console.log(this.id);
        this.loadData();
    }
    CreateinvoicePage.prototype.gocreate = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Are you sure ?',
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.createinvoice();
                    }
                }
            ]
        });
        confirm.present();
    };
    CreateinvoicePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://zonepage.gr/gymapp/appcrm/getcustomers.php');
        data.subscribe(function (result) {
            _this.items = result;
            console.log(_this.items);
        });
    };
    CreateinvoicePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CreateinvoicePage');
    };
    CreateinvoicePage.prototype.createinvo = function () {
        console.log(this.cost);
    };
    CreateinvoicePage.prototype.createinvoice = function () {
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/invoices/pdf.php';
        var postData = new FormData();
        postData.append('cost', this.cost);
        postData.append('invoceid', this.id);
        postData.append('product', this.service);
        postData.append('date', this.date);
        postData.append('name', this.customer);
        postData.append('othername', this.othercustomer);
        postData.append('payment', this.payment);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //this.storage.set('id', data);		 
        });
    };
    CreateinvoicePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-createinvoice',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/createinvoice/createinvoice.html"*/'<!--\n  Generated template for the CreateinvoicePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>createinvoice</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-list>\n	\n	 <ion-item>\n    <ion-label color="primary" stacked>Invoice Id</ion-label>\n    <ion-input type="text" placeholder="ID" [(ngModel)]="id"></ion-input>\n  </ion-item>\n\n   <ion-item>\n   <ion-label color="primary">Επιλογή Πελάτη</ion-label>\n  <ion-select [(ngModel)]="customer">\n    <ion-option *ngFor="let field of items" value = "{{field.reg_id}}">{{field.lastname}} {{field.fullname}}</ion-option>\n    </ion-select>\n    </ion-item>\n	\n\n	   <ion-item>\n    <ion-label color="primary" stacked>Όνομα μη Καταγεγραμμένου Πελάτη</ion-label>\n    <ion-input type="text" placeholder="Όνομα Πελάτη " [(ngModel)]="othercustomer"></ion-input>\n  </ion-item>\n	\n  <ion-item>\n    <ion-label color="primary" stacked>Ημερομηνία Έκδοσης</ion-label>\n     <ion-datetime displayFormat="DD/MM/YYYY" [(ngModel)]="date"></ion-datetime>\n  </ion-item>\n  \n   <ion-item>\n    <ion-label color="primary" stacked>Υπηρεσία</ion-label>\n    <ion-input type="text" placeholder="Υπηρεσία" [(ngModel)]="service"></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label>Τρόπος Πληρωμής</ion-label>\n    <ion-select [(ngModel)]="payment" multiple="false" cancelText="Cancel" okText="Ok">\n       <ion-option value = "Μετρητά" selected>Μετρητά</ion-option>\n      <ion-option value = "Καταθεση σε Τράπεζα">Καταθεση σε Τράπεζα</ion-option>\n      <ion-option value = "Με Κάρτα">Με Κάρτα</ion-option>\n       \n     \n    </ion-select>\n  </ion-item>\n  \n  \n     <ion-item>\n    <ion-label color="primary" stacked>Χρηματικό Ποσό με ΦΠΑ</ion-label>\n    <ion-input type="number" placeholder="0" [(ngModel)]="cost"></ion-input>\n  </ion-item>\n  \n  \n  \n	</ion-list>\n<button ion-button  end (click)="gocreate()">CreateInvoice</button>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/createinvoice/createinvoice.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], CreateinvoicePage);
    return CreateinvoicePage;
}());

//# sourceMappingURL=createinvoice.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CutomersstatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the CutomersstatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CutomersstatsPage = /** @class */ (function () {
    function CutomersstatsPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadData();
        this.toploadData();
    }
    CutomersstatsPage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://zonepage.gr/gymapp/appcrm/customerstats.php');
        data.subscribe(function (result) {
            _this.items = result;
            console.log(result);
            _this.total = result.length;
        });
    };
    CutomersstatsPage.prototype.toploadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://zonepage.gr/gymapp/appcrm/topclients.php');
        data.subscribe(function (result) {
            _this.topitems = result;
            console.log(result);
            _this.toptotal = result.length;
        });
    };
    CutomersstatsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CutomersstatsPage');
    };
    CutomersstatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-cutomersstats',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/cutomersstats/cutomersstats.html"*/'<!--\n  Generated template for the CutomersstatsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Στατιστικά Πελατών</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-grid>\n<ion-row>\n<ion-col col-lg-2>\n<h2>Ενεργοί Πελάτες</h2>\n<p>Ενεργός Πελάτης θεωρείται αυτός που έχει ενεργό πρόγραμμα και έχει κλείσει τουλάχιστον 1 Ραντεβού τον τελευταίο μήνα</p>\n<h2>Σύνολο: {{total}}</h2>\n\n<ion-list>\n    <ion-item *ngFor="let item of items" (click)="gotoproduct(item)">\n    \n	<ion-grid>\n  <ion-row>\n   \n    <ion-col col-lg-12><h2>{{item.fullname}}  {{item.lastname}}</h2>\n	  {{item.Points}} Credits\n	  <p>Επισκέψεις τον τελευταίο μήνα: {{item.episkepseis}}</p></ion-col>\n	  \n  </ion-row>\n</ion-grid>\n	\n	\n      \n     \n    </ion-item>\n  </ion-list>\n  </ion-col>\n  \n  \n  <ion-col col-lg-2>\n<h2>Top 10 Πελάτες</h2>\n<p>Περισσότερες επισκέψεις τις τελευταίες 30 ημέρες</p>\n<h2>Σύνολο: {{toptotal}}</h2>\n\n\n  </ion-col>\n   \n  \n  \n    <ion-col col-lg-2>\n<ion-list>\n    <ion-item *ngFor="let item of topitems" (click)="gotoproduct(item)">\n    \n	<ion-grid>\n  <ion-row>\n   \n    <ion-col col-lg-12><h2>{{item.fullname}}  {{item.lastname}}</h2>\n	  {{item.Points}} Credits\n	  <p>Επισκέψεις τον τελευταίο μήνα: {{item.episkepseis}}</p></ion-col>\n	  \n  </ion-row>\n</ion-grid>\n	\n	\n      \n     \n    </ion-item>\n  </ion-list>\n  </ion-col>\n  \n  \n    </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/cutomersstats/cutomersstats.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], CutomersstatsPage);
    return CutomersstatsPage;
}());

//# sourceMappingURL=cutomersstats.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateshistoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the DateshistoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var DateshistoryPage = /** @class */ (function () {
    function DateshistoryPage(navCtrl, navParams, http, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.programfilter = 0;
        this.getusercome(0);
    }
    DateshistoryPage_1 = DateshistoryPage;
    DateshistoryPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad DateshistoryPage');
    };
    DateshistoryPage.prototype.getusercome = function (value) {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('category', value);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/historycomingusers2.php', postData);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    DateshistoryPage.prototype.onChange = function (value) {
        console.log(value);
        this.programfilter = value;
        this.getusercome(this.programfilter);
    };
    DateshistoryPage.prototype.gotoprogram = function (item) {
        var _this = this;
        console.log(item);
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε να ακυρώσετε το ραντεβού για ' + name,
            message: 'Επιλέγοντα "ΝΑΙ" επιστρέφονται στον πελάτη τα μισά Credits. Επιλέγοντας ναι με επιστροφή Credits επιστρέφονται όλα τα Credits',
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'ΝΑΙ ',
                    handler: function () {
                        _this.returncredits = (parseInt(item.costcredits) / 2).toFixed(0);
                        _this.deleteregister(item, _this.returncredits);
                        console.log(_this.returncredits);
                    }
                },
                {
                    text: 'ΝΑΙ Επιστροφή Credits',
                    handler: function () {
                        _this.deleteregister(item, item.costcredits);
                    }
                },
                { text: 'Να μην Επιστραφούν',
                    handler: function () {
                        _this.deleteregister(item, '0');
                    } }
            ]
        });
        confirm.present();
    };
    DateshistoryPage.prototype.deleteregister = function (item, returnc) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/deleteregister.php';
        var postData = new FormData();
        postData.append('prodid', item.progid);
        postData.append('userid', item.reg_id);
        postData.append('return', returnc);
        postData.append('person_id', item.person_id);
        postData.append('id', item.cpid);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Add reminder		 
            if (result == 'Registration Complete') {
                _this.navCtrl.setRoot(DateshistoryPage_1);
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    DateshistoryPage = DateshistoryPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-dateshistory',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/dateshistory/dateshistory.html"*/'<!--\n  Generated template for the DateshistoryPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Ιστορικό Ραντεβού</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-grid>\n  <ion-row>\n  \n \n  \n<ion-col col-md-6>\n<ion-list class="Filter">\n  <ion-item>\n    <ion-label>Επιλέξτε πρόγραμμα</ion-label>\n    <ion-select [(ngModel)]="toppings" multiple="false" cancelText="Cancel" okText="Ok" (ionChange)="onChange($event)">\n       <ion-option value = "0" selected>All</ion-option>\n      <ion-option value = "2">Personal Training</ion-option>\n      <ion-option value = "3">Pilates Mat</ion-option>\n       <ion-option value = "4">Pilates Tower</ion-option>\n      <ion-option value = "5">Yoga</ion-option>\n       <ion-option value = "6">TRX Training</ion-option>\n        <ion-option value = "7">Stretching</ion-option>\n         <ion-option value = "8">HIPS & ABS</ion-option>\n         <ion-option value = "9">Trampoline</ion-option>\n     \n    </ion-select>\n  </ion-item>\n</ion-list>\n</ion-col>\n\n\n\n</ion-row>\n</ion-grid>\n\n <h2>Ραντεβού</h2> \n	<p>Μπορείτε να ακυρώσετε συγκεκριμένο ραντεβού με Click</p>\n	 <ion-list>\n\n <ion-item *ngFor="let item of users" (click)="gotoprogram(item)">\n    <h3>{{item.fullname}}  {{item.lastname}}</h3> \n	<p>{{item.tilte}} - Trainer: {{item.person}}</p>\n	<p>{{item.timehour1}}</p>\n    </ion-item> \n\n\n\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/dateshistory/dateshistory.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], DateshistoryPage);
    return DateshistoryPage;
    var DateshistoryPage_1;
}());

//# sourceMappingURL=dateshistory.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InvoicesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__createinvoice_createinvoice__ = __webpack_require__(176);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the InvoicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var InvoicesPage = /** @class */ (function () {
    function InvoicesPage(navCtrl, navParams, http, iab, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.iab = iab;
        this.alertCtrl = alertCtrl;
        this.loadData();
    }
    InvoicesPage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get('https://zonepage.gr/gymapp/appcrm/getinvoices.php');
        data.subscribe(function (result) {
            _this.items = result;
            console.log(result);
            _this.total = result.length;
        });
    };
    InvoicesPage.prototype.gosend = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε να γίνει αποστολή των τιμολογίων;',
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'YES',
                    handler: function () {
                        console.log(_this.date);
                        var data;
                        var url = 'http://zonepage.gr/gymapp/appcrm/invoices/sendinvoices.php';
                        var postData = new FormData();
                        postData.append('date', _this.date);
                        data = _this.http.post(url, postData);
                        data.subscribe(function (result) {
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    InvoicesPage.prototype.openWebpage = function (item) {
        var num = item.path;
        var url = "https://zonepage.gr/gymapp/appcrm/invoices/" + num;
        // Opening a URL and returning an InAppBrowserObject
        var browser = this.iab.create(url, '_blank');
        // Inject scripts, css and more with browser.X
    };
    InvoicesPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad InvoicesPage');
    };
    InvoicesPage.prototype.gocreate = function () {
        console.log(this.items[0].invoiceid);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__createinvoice_createinvoice__["a" /* CreateinvoicePage */], { item: this.items[0].invoiceid });
    };
    InvoicesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-invoices',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/invoices/invoices.html"*/'<!--\n  Generated template for the InvoicesPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>invoices</ion-title>\n	<ion-buttons end>\n<button ion-button end (click)="gocreate()">Create New Invoice</button>\n</ion-buttons>\n  </ion-navbar>\n  \n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-grid>\n<ion-row>\n\n<ion-col col-lg-2>\n<p>Αποστολή Τιμολογιών σε Λογιστήριο</p>\n<p>Επιλέξτε Μήνα</p>\n  <ion-datetime displayFormat="MM/YYYY" [(ngModel)]="date"></ion-datetime>\n  <button ion-button  end (click)="gosend()">Αποστολή</button>\n  \n </ion-col>\n<ion-col col-lg-10>\n<ion-list>\n    <ion-item *ngFor="let item of items">\n    \n	<ion-grid>\n  <ion-row>\n   <ion-col col-lg-2>{{item.invoiceid}}</ion-col>\n    <ion-col col-lg-2><h2>{{item.fullname}} {{item.lastname}} {{item.othername}}</h2>\n	  </ion-col>\n\n    <ion-col col-lg-2>{{item.cost}} €</ion-col>\n	<ion-col col-lg-2>{{item.date}}</ion-col>\n	<ion-col col-lg-2><button ion-button block clear (click)="openWebpage(item)">Open invoice</button></ion-col>\n		\n  </ion-row>\n</ion-grid>\n	\n	\n      \n     \n    </ion-item>\n  </ion-list>\n</ion-col>\n   </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/invoices/invoices.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], InvoicesPage);
    return InvoicesPage;
}());

//# sourceMappingURL=invoices.js.map

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VieworderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__orders_orders__ = __webpack_require__(94);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the VieworderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var VieworderPage = /** @class */ (function () {
    function VieworderPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.datas = this.navParams.get('item');
        console.log(this.datas);
        if (this.datas.gateaway == '1') {
            this.gateaway = "Πληρωμή μέσω Τραπέζης";
        }
        if (this.datas.gateaway == '2') {
            this.gateaway = "Πληρωμή Mendez";
        }
    }
    VieworderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad VieworderPage');
    };
    VieworderPage.prototype.completeorder = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/getorders.php';
        var postData = new FormData();
        postData.append('status', 'complete');
        postData.append('orderid', this.datas.orderid);
        postData.append('userid', this.datas.reg_id);
        postData.append('productid', this.datas.productid);
        postData.append('months', this.datas.months);
        postData.append('freecredits', this.datas.freecredits);
        postData.append('credits', this.datas.credits);
        postData.append('price', this.datas.amount);
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items = data;
            console.log(data);
            //this.storage.set('id', data);		 
            //console.log (this.items[category]);
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__orders_orders__["a" /* OrdersPage */]);
    };
    VieworderPage.prototype.deleteorder = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/getorders.php';
        var postData = new FormData();
        postData.append('status', 'delete');
        postData.append('orderid', this.datas.orderid);
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items = data;
            console.log(data);
            //this.storage.set('id', data);		 
            //console.log (this.items[category]);
        });
    };
    VieworderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-vieworder',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/vieworder/vieworder.html"*/'<!--\n  Generated template for the VieworderPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>vieworder</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n<ion-grid>\n  <ion-row>\n  <ion-col col-6>\n<h2 padding>Id #{{datas.orderid}} - {{datas.created}}</h2>\n<h2 padding>Ονομα: {{datas.fullname}} {{datas.lastname}}</h2>\n<h2 padding>Προίόν: {{datas.title}}</h2>\n<h2 padding>Κόστος: {{datas.amount}}</h2>\n\n<h2 padding >{{gateaway}}</h2>\n\n</ion-col>\n<ion-col col-6>\n<button ion-button block (click)="completeorder()" *ngIf="datas.orderstatus === \'0\'">Ολοκλήρωση Παραγγελίας</button>\n<button ion-button block (click)="deleteorder()">Ακύρωση Παραγγελίας</button>\n</ion-col>\n </ion-row>\n</ion-grid>\n \n\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/vieworder/vieworder.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], VieworderPage);
    return VieworderPage;
}());

//# sourceMappingURL=vieworder.js.map

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainesPrivaPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the TrainesPrivaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrainesPrivaPage = /** @class */ (function () {
    function TrainesPrivaPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.giannis = [];
        this.nikos = [];
        this.ksenia = [];
        this.elena = [];
        this.giorgos = [];
        this.eirini = [];
    }
    TrainesPrivaPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainesPrivaPage');
        this.initializeCalendarOptions();
        this.trainers(0, 0);
    };
    TrainesPrivaPage.prototype.trainers = function ($month, $year) {
        var _this = this;
        this.giannispersons = 0;
        this.nikospersons = 0;
        this.giorgospersons = 0;
        this.kseniapersons = 0;
        this.elenapersons = 0;
        this.eirinipersons = 0;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/newweeklyprogram.php';
        var postData = new FormData();
        postData.append('id', 'all');
        postData.append('month', $month);
        postData.append('year', $year);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.users = result;
            var i;
            for (var _i = 0, _a = _this.users; _i < _a.length; _i++) {
                var user = _a[_i];
                if (user['trainer_id'] == 1) {
                    _this.giannis.push(user);
                    _this.giannispersons = _this.giannispersons + user['countper'];
                }
                if (user['trainer_id'] == 2) {
                    _this.giorgos.push(user);
                    _this.giorgospersons = _this.giorgospersons + user['countper'];
                }
                if (user['trainer_id'] == 3) {
                    _this.nikos.push(user);
                    _this.nikospersons = _this.nikospersons + user['countper'];
                }
                if (user['trainer_id'] == 4) {
                    _this.elena.push(user);
                    _this.elenapersons = _this.elenapersons + user['countper'];
                }
                if (user['trainer_id'] == 5) {
                    _this.ksenia.push(user);
                    _this.kseniapersons = _this.kseniapersons + user['countper'];
                }
                if (user['trainer_id'] == 6) {
                    _this.eirini.push(user);
                    _this.eirinipersons = _this.eirinipersons + user['countper'];
                }
            }
            _this.gianniscount = _this.giannis.length;
            _this.nikoscount = _this.nikos.length;
            _this.giorgoscount = _this.giorgos.length;
            _this.elenacount = _this.elena.length;
            _this.kseniacount = _this.ksenia.length;
            _this.eirinicount = _this.eirini.length;
            console.log(_this.giannis);
        });
    };
    TrainesPrivaPage.prototype.deletetrainer = function ($id) {
        console.log($id);
    };
    TrainesPrivaPage.prototype.initializeCalendarOptions = function () {
        this.calendarOptions = {
            header: {
                left: 'title',
                right: 'month,agendaWeek,agendaDay,agendaFourDay,'
            },
            defaultTimedEventDuration: '01:00:00',
            forceEventDuration: true,
            footer: {
                right: 'today prev,next',
            },
            views: {
                agendaFourDay: {
                    type: 'listYear',
                    buttonText: 'All'
                }
            },
            //   theme:'jquery-ui',
            height: 700,
            fixedWeekCount: false,
            editable: true,
            allDay: true,
            dayClick: function (date, jsEvent, view, resourceObj) {
                console.log(__WEBPACK_IMPORTED_MODULE_3_moment__(date, 'YYYY-MM-DD').toDate());
                var dateto = __WEBPACK_IMPORTED_MODULE_3_moment__(date, 'YYYY-MM-DD').toDate();
            },
            eventLimit: true,
            events: { url: 'https://zonepage.gr/gymapp/appcrm/private-trainers.php'
            },
            timeFormat: 'HH:mm',
        };
    };
    TrainesPrivaPage.prototype.changed = function ($event) {
        console.log($event.month);
        this.giannis = [];
        this.giorgos = [];
        this.nikos = [];
        this.elena = [];
        this.ksenia = [];
        this.eirini = [];
        this.trainers($event.month, $event.year);
    };
    TrainesPrivaPage.prototype.addtrainer = function () {
        console.log(this.person212);
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/addtrainer_pri.php';
        var postData = new FormData();
        postData.append('person', this.person212);
        postData.append('timehour', this.time2);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
        });
    };
    TrainesPrivaPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-traines-priva',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/traines-priva/traines-priva.html"*/'<!--\n  Generated template for the TrainesPrivaPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n  	 <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Στατιστικά Γυμναστών</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n	<h2></h2>\n	<h3>Ενεργές Ώρες</h3>\n	\n <ion-list>\n	<ion-item>\n  <ion-label>Μήνας Χρόνος</ion-label>\n  <ion-datetime displayFormat="MMMM YYYY" max="2022"  (ionChange)="changed($event)"></ion-datetime>\n</ion-item>\n</ion-list>\n<ion-grid>\n  <ion-row>\n    <ion-col col-lg-3>\n    	<h3 class="dividertitle" >Γιαννης : {{gianniscount}} | {{giannispersons}} Persons</h3>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of giannis">\n	\n	\n      <p class="dateicon">{{item.title }}</p>\n	  <p class="mmicon">{{item.timehour | date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.countper}} Persons</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n\n\n    <ion-col col-lg-3>\n    	<h3 class="dividertitle" >Nikos : {{nikoscount}} | {{nikospersons}} Persons</h3>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of nikos">\n	\n	\n      <p class="dateicon">{{item.title }}</p>\n	  <p class="mmicon">{{item.timehour | date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.countper}} Persons</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n\n\n\n    <ion-col col-lg-3>\n    	<h3 class="dividertitle" >Γιώργος : {{giorgoscount}} | {{giorgospersons}} Persons</h3>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of giorgos">\n	\n	\n      <p class="dateicon">{{item.title }}</p>\n	  <p class="mmicon">{{item.timehour | date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.countper}} Persons</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n\n    <ion-col col-lg-3>\n    	<h3 class="dividertitle" >Ειρήνη : {{eirinicount}} | {{eirinipersons}} Persons </h3>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of eirini">\n	\n	\n      <p class="dateicon">{{item.title }}</p>\n	  <p class="mmicon">{{item.timehour | date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.countper}} Persons</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n  <ion-col col-lg-3>\n        	<h3 class="dividertitle" >Ελενα : {{elenacount}} | {{elenapersons}} Persons</h3>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of elena">\n	\n	\n      <p class="dateicon">{{item.title }}</p>\n	  <p class="mmicon">{{item.timehour | date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.countper}} Persons</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n\n\n <ion-col col-lg-3>\n        	<h3 class="dividertitle" >Ξενια : {{kseniacount}} | {{kseniapersons}} Persons</h3>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of ksenia">\n	\n	\n      <p class="dateicon">{{item.title }}</p>\n	  <p class="mmicon">{{item.timehour | date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.countper}} Persons</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n\n\n\n</ion-row>\n</ion-grid>\n\n<ion-grid>\n<ion-row>\n\n\n<ion-col col-lg-8>\n	<h2>Ιδιωτικά Ραντεβού</h2>\n\n	<angular2-fullcalendar id="myCalendar" [options]="calendarOptions"  (eventClick)="deletetrainer($event)" ></angular2-fullcalendar>\n</ion-col>\n\n\n\n<ion-col col-lg-4>\n<h2>Προσθήκη Ραντεβού Χειροκίνητα</h2>\n\n<ion-list>\n  \n  <ion-item>\n     <ion-label color="primary">Επιλογή Γυμναστή</ion-label>\n  <ion-select [(ngModel)]="person212">\n    <ion-option value="0">Κανένας</ion-option>\n    <ion-option value="33">Γιάννης</ion-option>\n    <ion-option value="132">Γιώργος</ion-option>\n    <ion-option value="144">Νίκος</ion-option>\n    <ion-option value="158">Έλενα</ion-option>\n	  <ion-option value="173">Ξένια</ion-option>\n    <ion-option value="6">Ειρήνη</ion-option>\n    \n  \n  </ion-select>\n  </ion-item>\n\n\n\n\n\n <ion-item>\n  <ion-label>Επιλογή Ώρας</ion-label>\n  <ion-datetime displayFormat="DD/MM/YYYY HH:mm" [(ngModel)]="time2"></ion-datetime>\n</ion-item>\n\n \n\n\n  \n</ion-list>\n\n<button ion-button (click)="addtrainer()">Προσθήκη</button>\n</ion-col>\n</ion-row>\n</ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/traines-priva/traines-priva.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], TrainesPrivaPage);
    return TrainesPrivaPage;
}());

//# sourceMappingURL=traines-priva.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WatersbuyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the WatersbuyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WatersbuyPage = /** @class */ (function () {
    function WatersbuyPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.waters(0, 0);
        this.getparaggel();
    }
    WatersbuyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WatersbuyPage');
    };
    WatersbuyPage.prototype.waters = function ($month, $year) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/watersbuy.php';
        var postData = new FormData();
        postData.append('month', $month);
        postData.append('year', $year);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.userswater = result;
            _this.monthsels = 0;
            for (var _i = 0, _a = _this.userswater; _i < _a.length; _i++) {
                var user = _a[_i];
                _this.monthsels = _this.monthsels + user['qnty'] * 0.5;
                _this.monthselsmoney = _this.monthsels / 0.5;
            }
            console.log(_this.monthsels);
        });
    };
    WatersbuyPage.prototype.changed = function ($event) {
        console.log($event.month);
        this.userswater = [];
        this.waters($event.month, $event.year);
    };
    WatersbuyPage.prototype.getparaggel = function () {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/addwaters_para.php';
        var postData = new FormData();
        postData.append('do', 'get');
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.parag = result;
            _this.Totalmonthsels = _this.parag[0]['Totalwaters'];
            _this.Totalmonthselsmoney = _this.parag[0]['Totalwatersbot'];
            _this.stock = _this.Totalmonthsels - _this.Totalmonthselsmoney;
            console.log(_this.stock);
        });
    };
    WatersbuyPage.prototype.addtrainer = function () {
        console.log(this.waterprice);
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/addwaters_para.php';
        var postData = new FormData();
        postData.append('do', 'add');
        postData.append('watersprice', this.waterprice);
        postData.append('waterscount', this.waterscount);
        postData.append('timehour', this.time2);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            console.log(result);
        });
    };
    WatersbuyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-watersbuy',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/watersbuy/watersbuy.html"*/'<!--\n  Generated template for the WatersbuyPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n<ion-navbar>\n  	 <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Στατιστικά Νερών</ion-title>\n  </ion-navbar>\n\n\n</ion-header>\n\n\n<ion-content padding>\n	<ion-grid>\n  <ion-row>\n  	<div col-lg-6>\n  		<h2>Σύνολο Πωλήσεων τον μήνα {{monthselsmoney}} Μπουκάλια - {{monthsels}} €</h2>\n  		<h2>Προσθήκη Παραγγελίας</h2>\n	 <ion-list>\n	<ion-item>\n  <ion-label>Μήνας Χρόνος</ion-label>\n  <ion-datetime displayFormat="MMMM YYYY" max="2022"  (ionChange)="changed($event)"></ion-datetime>\n</ion-item>\n</ion-list>\n\n<ion-grid>\n  <ion-row>\n	    <ion-col col-lg-12>\n    	\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of userswater">\n	\n	\n      <p class="dateicon">{{item.fullname}}  {{item.lastname}}</p>\n	  <p class="mmicon">{{item.timestamp| date: "dd-MM-yyyy hh:mm"}}</p>\n    <p class="mmicon">{{item.qnty}} waters - {{item.total}} €</p>\n</ion-item>\n</ion-list>\n    </ion-col>\n\n</ion-row>\n</ion-grid>\n</div>\n\n<div col-lg-6>\n\n<h2>Απόθεμα {{stock}} Μπουκάλια - Έχετε Πουλήσει {{Totalmonthselsmoney}} -Τζίρος {{Totalmonthselsmoney*0.5}} €</h2>\n<h2>Κέρδος  {{Totalmonthselsmoney*0.5 - Totalmonthsels*0.2}} €</h2>\n\n\n<h2>Προσθήκη Παραγγελίας</h2>\n\n<ion-list>\n  \n <ion-item>\n\n <ion-label color="primary" stacked>Τιμή Νερού (Χονδρική χωρίς το € ΠΡΟΣΟΧΗ η υποδιαστολή είναι με τελεία)</ion-label>\n          <ion-input type="text" placeholder="0" [(ngModel)]="waterprice"></ion-input>\n\n </ion-item>	\n\n     <ion-item>\n          <ion-label color="primary" stacked>Αριθμός Μπουκαλιών</ion-label>\n          <ion-input type="number" placeholder="00000" [(ngModel)]="waterscount"></ion-input>\n        </ion-item>\n\n\n <ion-item>\n  <ion-label>Επιλογή Ημέρας</ion-label>\n  <ion-datetime displayFormat="DD/MM/YYYY HH:mm" [(ngModel)]="time2"></ion-datetime>\n</ion-item>\n\n \n\n\n  \n</ion-list>\n\n<button ion-button (click)="addtrainer()">Προσθήκη</button>\n\n\n<ion-grid>\n  <ion-row>\n	    <ion-col col-lg-12>\n    	<h2>Παραγγελίες Νερών</h2>\n    	 <ion-list class="products">\n    <ion-item *ngFor="let item of parag">\n	\n	\n      <p class="dateicon">{{item.waterscount}} Μπουκάλια</p>\n       <p class="dateicon">Ημέρα Παραγγελίας {{item.time | date: "dd-MM-yyyy hh:mm"}}</p>\n	  <p class="mmicon">Κόστος {{item.totalprice}}  €</p>\n\n</ion-item>\n</ion-list>\n    </ion-col>\n\n</ion-row>\n</ion-grid>\n\n</div>\n\n\n\n\n</ion-row>\n</ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/watersbuy/watersbuy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], WatersbuyPage);
    return WatersbuyPage;
}());

//# sourceMappingURL=watersbuy.js.map

/***/ }),

/***/ 193:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 193;

/***/ }),

/***/ 237:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/addclass/addclass.module": [
		851,
		28
	],
	"../pages/addcustomer/addcustomer.module": [
		852,
		27
	],
	"../pages/addproduct/addproduct.module": [
		853,
		3
	],
	"../pages/addprogram/addprogram.module": [
		335
	],
	"../pages/addtarget/addtarget.module": [
		854,
		26
	],
	"../pages/classes/classes.module": [
		855,
		25
	],
	"../pages/createinvoice/createinvoice.module": [
		856,
		24
	],
	"../pages/customers/customers.module": [
		857,
		23
	],
	"../pages/customerview/customerview.module": [
		879,
		22
	],
	"../pages/cutomersstats/cutomersstats.module": [
		858,
		21
	],
	"../pages/dateshistory/dateshistory.module": [
		859,
		20
	],
	"../pages/editclass/editclass.module": [
		860,
		19
	],
	"../pages/editcustomerprogram/editcustomerprogram.module": [
		861,
		18
	],
	"../pages/editproduct/editproduct.module": [
		862,
		2
	],
	"../pages/editprogram/editprogram.module": [
		863,
		17
	],
	"../pages/hometrainers/hometrainers.module": [
		864,
		1
	],
	"../pages/invoices/invoices.module": [
		865,
		16
	],
	"../pages/login/login.module": [
		867,
		15
	],
	"../pages/orders/orders.module": [
		866,
		14
	],
	"../pages/products/products.module": [
		868,
		0
	],
	"../pages/requestforuser/requestforuser.module": [
		464
	],
	"../pages/sendzoom/sendzoom.module": [
		465
	],
	"../pages/targets/targets.module": [
		870,
		13
	],
	"../pages/trainerhome/trainerhome.module": [
		869,
		12
	],
	"../pages/traines-priva/traines-priva.module": [
		871,
		11
	],
	"../pages/useraddmetric/useraddmetric.module": [
		872,
		10
	],
	"../pages/usermetrics/usermetrics.module": [
		875,
		9
	],
	"../pages/viewmetrics/viewmetrics.module": [
		874,
		8
	],
	"../pages/vieworder/vieworder.module": [
		873,
		7
	],
	"../pages/viewtarget/viewtarget.module": [
		876,
		6
	],
	"../pages/watersbuy/watersbuy.module": [
		878,
		5
	],
	"../pages/weeklyprograms/weeklyprograms.module": [
		877,
		4
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 237;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImagesProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ImagesProvider = /** @class */ (function () {
    function ImagesProvider(http) {
        this.http = http;
        /**
         * @name _READER
         * @type object
         * @private
         * @description              Creates a FileReader API object
         */
        this._READER = new FileReader();
        /**
         * @name _REMOTE_URI
         * @type String
         * @private
         * @description              The URI for the remote PHP script that will handle the image upload/parsing
         */
        this._REMOTE_URI = "http://YOUR-REMOTE-URI-HERE/parse-upload.php";
    }
    /**
     * @public
     * @method handleImageSelection
     * @param event  {any}     	The DOM event that we are capturing from the File input field
     * @description    			Uses the FileReader API to capture the input field event, retrieve
     *                 			the selected image and return that as a base64 data URL courtesy of
     *							an Observable
     * @return {Observable}
     */
    ImagesProvider.prototype.handleImageSelection = function (event) {
        var _this = this;
        var file = event.target.files[0];
        this._READER.readAsDataURL(file);
        return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].create(function (observer) {
            _this._READER.onloadend = function () {
                observer.next(_this._READER.result);
                observer.complete();
            };
        });
    };
    /**
     * @public
     * @method isCorrectFile
     * @param file  {String}     The file type we want to check
     * @description    			Uses a regular expression to check that the supplied file format
     *                 			matches those specified within the method
     * @return {any}
     */
    ImagesProvider.prototype.isCorrectFileType = function (file) {
        return (/^(gif|jpg|jpeg|png)$/i).test(file);
    };
    /**
     * @public
     * @method uploadImageSelection
     * @param file  		{String}    The file data to be uploaded
     * @param mimeType  	{String}    The file's MimeType (I.e. jpg, gif, png etc)
     * @description    				Uses the Angular HttpClient to post the data to a remote
     *                 				PHP script, returning the observable to the parent script
     * 								allowing that to be able to be subscribed to
     * @return {any}
     */
    ImagesProvider.prototype.uploadImageSelection = function (file, mimeType) {
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ 'Content-Type': 'application/octet-stream' }), fileName = Date.now() + '.' + mimeType, options = { "name": fileName, "file": file };
        return this.http.post(this._REMOTE_URI, JSON.stringify(options), headers);
    };
    ImagesProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ImagesProvider);
    return ImagesProvider;
}());

//# sourceMappingURL=images.js.map

/***/ }),

/***/ 312:
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__(769);
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(48)))

/***/ }),

/***/ 335:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddprogramPageModule", function() { return AddprogramPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addprogram__ = __webpack_require__(139);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddprogramPageModule = /** @class */ (function () {
    function AddprogramPageModule() {
    }
    AddprogramPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__addprogram__["a" /* AddprogramPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__addprogram__["a" /* AddprogramPage */]),
            ],
        })
    ], AddprogramPageModule);
    return AddprogramPageModule;
}());

//# sourceMappingURL=addprogram.module.js.map

/***/ }),

/***/ 464:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RequestforuserPageModule", function() { return RequestforuserPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__requestforuser__ = __webpack_require__(140);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RequestforuserPageModule = /** @class */ (function () {
    function RequestforuserPageModule() {
    }
    RequestforuserPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__requestforuser__["a" /* RequestforuserPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__requestforuser__["a" /* RequestforuserPage */]),
            ],
        })
    ], RequestforuserPageModule);
    return RequestforuserPageModule;
}());

//# sourceMappingURL=requestforuser.module.js.map

/***/ }),

/***/ 465:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendzoomPageModule", function() { return SendzoomPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sendzoom__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SendzoomPageModule = /** @class */ (function () {
    function SendzoomPageModule() {
    }
    SendzoomPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sendzoom__["a" /* SendzoomPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sendzoom__["a" /* SendzoomPage */]),
            ],
        })
    ], SendzoomPageModule);
    return SendzoomPageModule;
}());

//# sourceMappingURL=sendzoom.module.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeeklyprogramsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addprogram_addprogram__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__requestforuser_requestforuser__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the WeeklyprogramsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WeeklyprogramsPage = /** @class */ (function () {
    function WeeklyprogramsPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.mondays = [];
        this.tusdays = [];
        this.wensdays = [];
        this.thursdays = [];
        this.fridays = [];
        this.saturdays = [];
        this.sundays = [];
        this.second = 'inactive';
        this.thirtd = 'inactive';
        this.four = 'inactive';
        this.five = 'inactive';
        this.six = 'inactive';
        this.seven = 'inactive';
        this.testprogram = [];
    }
    WeeklyprogramsPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad WeeklyprogramsPage');
        this.initializeCalendarOptions();
    };
    WeeklyprogramsPage.prototype.addprogram = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__addprogram_addprogram__["a" /* AddprogramPage */]);
    };
    WeeklyprogramsPage.prototype.gotoprogram = function (item) {
        console.log(item.progid);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__requestforuser_requestforuser__["a" /* RequestforuserPage */], { item: item });
    };
    WeeklyprogramsPage.prototype.onChange = function (value) {
        console.log(value);
        this.programfilter = value;
    };
    WeeklyprogramsPage.prototype.initializeCalendarOptions = function () {
        this.calendarOptions = {
            header: {
                left: 'title',
                right: 'month,agendaWeek,agendaDay,agendaFourDay,'
            },
            defaultTimedEventDuration: '01:00:00',
            forceEventDuration: true,
            footer: {
                right: 'today prev,next',
            },
            views: {
                agendaFourDay: {
                    type: 'listYear',
                    buttonText: 'All'
                }
            },
            //   theme:'jquery-ui',
            height: 700,
            fixedWeekCount: false,
            editable: true,
            allDay: true,
            dayClick: function (date, jsEvent, view, resourceObj) {
                console.log(__WEBPACK_IMPORTED_MODULE_5_moment__(date, 'YYYY-MM-DD').toDate());
                var dateto = __WEBPACK_IMPORTED_MODULE_5_moment__(date, 'YYYY-MM-DD').toDate();
                window.location.assign('#/some-othr-path/' + dateto);
            },
            eventLimit: true,
            events: { url: 'https://zonepage.gr/gymapp/appcrm/newweeklyprogram2.php'
            },
            timeFormat: 'HH:mm',
            eventClick: function (calEvent, jsEvent) {
                var testev = [];
                testev.push({
                    progid: calEvent.progid,
                    tilte: calEvent.title,
                    availibility: calEvent.availibility,
                    costcredits: calEvent.costcredits,
                    time1: calEvent.time1,
                    timehour1: calEvent.start,
                    category: calEvent.category
                });
                console.log(JSON.stringify(testev));
                localStorage.setItem('EventData', calEvent.title);
                window.location.assign('#/some-other-path/' + JSON.stringify(testev));
            }
        };
    };
    WeeklyprogramsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-weeklyprograms',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/weeklyprograms/weeklyprograms.html"*/'<!--\n  Generated template for the WeeklyprogramsPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>weeklyprograms</ion-title>\n	 <ion-buttons end>\n      <button ion-button primary (click)="addprogram()">\n       Προσθήκη\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n\n  <ion-grid>\n  <ion-row>\n<ion-col col-md-6>\n<ion-list class="Filter">\n  <ion-item>\n    <ion-label>Filter</ion-label>\n    <ion-select [(ngModel)]="toppings" multiple="false" cancelText="Cancel" okText="Ok" (ionChange)="onChange($event)">\n       <ion-option value = "0" selected>All</ion-option>\n      <ion-option value = "2">Personal Training</ion-option>\n      <ion-option value = "3">Pilates Mat</ion-option>\n       <ion-option value = "4">Pilates Tower</ion-option>\n      <ion-option value = "5">Yoga</ion-option>\n       <ion-option value = "6">TRX Training</ion-option>\n        <ion-option value = "7">Stretching</ion-option>\n         <ion-option value = "8">HIPS & ABS</ion-option>\n         <ion-option value = "9">Trampoline</ion-option>\n     \n    </ion-select>\n  </ion-item>\n</ion-list>\n</ion-col>\n</ion-row>\n\n  <angular2-fullcalendar id="myCalendar" [options]="calendarOptions"  ></angular2-fullcalendar>\n\n\n</ion-grid>\n\n<ion-grid>\n  <ion-row>\n    <ion-col col-lg-2 class="daycol">\n      <h2 text-center>{{dfirst}}</h2>\n	  <p>{{firsts | date: "dd/MM"}}</p>\n	  <ion-list>\n\n    <ion-item *ngFor="let item of mondays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n    <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n    </ion-col>\n    <ion-col col-md-2 class="daycol">\n     <h2 text-center>{{dtus}}</h2>\n		  <ion-list>\n\n    <ion-item *ngFor="let item of tusdays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n     <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n\n    </ion-col>\n    <ion-col col-md-2 class="daycol">\n      <h2 text-center>{{dwen}}</h2>\n	  		  <ion-list>\n\n    <ion-item *ngFor="let item of wensdays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n     <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n	\n    </ion-col>\n	 <ion-col col-md-2 class="daycol">\n      <h2 text-center>{{dthu}}</h2>\n	   <ion-list>\n\n    <ion-item *ngFor="let item of thursdays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n   <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n\n    </ion-col>\n	 <ion-col col-md-2 class="daycol">\n     <h2 text-center>{{dfri}}</h2>\n\n	 	   <ion-list>\n\n    <ion-item *ngFor="let item of fridays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n    <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n	 \n    </ion-col>\n	 <ion-col col-md-2 class="daycol">\n      <h2 text-center>{{dsat}}</h2>\n	   <ion-list>\n\n    <ion-item *ngFor="let item of saturdays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n    <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n	\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n\n    </ion-col>\n	\n	<ion-col col-md-2 class="daycol">\n      <h2 text-center>{{dsun}}</h2>\n	   <ion-list>\n\n    <ion-item *ngFor="let item of sundays" (click)="gotoprogram(item)">\n	<ng-container>\n	<ion-grid>\n    <ion-row>\n    <ion-col col-12><h2>{{item.tilte}}</h2></ion-col>\n    <ion-col col-12>Ώρα: {{item.timehour | date: "dd/MM HH:mm"}}</ion-col>\n	<ion-col col-12>Άτομα: {{item.availibility}}</ion-col>\n	\n   </ion-row>\n	\n	</ion-grid>\n	</ng-container>\n     \n    </ion-item> \n    \n</ion-list>\n\n    </ion-col>\n  </ion-row>\n</ion-grid>\n\n\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/weeklyprograms/weeklyprograms.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], WeeklyprogramsPage);
    return WeeklyprogramsPage;
}());

//# sourceMappingURL=weeklyprograms.js.map

/***/ }),

/***/ 48:
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || Function("return this")() || (1,eval)("this");
} catch(e) {
	// This works if the window reference is available
	if(typeof window === "object")
		g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 508:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AllexpiresPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__customerview_customerview__ = __webpack_require__(69);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the AllexpiresPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AllexpiresPage = /** @class */ (function () {
    function AllexpiresPage(navCtrl, navParams, http, storage, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.nowdate = new Date().toISOString();
        this.expireuser();
    }
    AllexpiresPage.prototype.expireuser = function () {
        var _this = this;
        var data;
        data = this.http.get('http://zonepage.gr/gymapp/appcrm/expireusersall.php');
        data.subscribe(function (result) {
            _this.expireusers = result;
            console.log(_this.expireusers);
        });
    };
    AllexpiresPage.prototype.notification = function (item) {
        var _this = this;
        console.log(item);
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Do you want to send a reminder to ' + name,
            buttons: [
                {
                    text: 'NO',
                    handler: function () {
                    }
                },
                {
                    text: 'YES',
                    handler: function () {
                        _this.sendreminder(item);
                    }
                }
            ]
        });
        confirm.present();
    };
    AllexpiresPage.prototype.sendreminder = function (item) {
        this.sendpushnotification(item);
        console.log(item);
        var url = 'https://mendez.gr/crm/expirepush.php';
        var postData = new FormData();
        postData.append('id', item.reg_id);
        postData.append('email', item.email);
        postData.append('description', item.description);
        postData.append('expires', item.timehour1);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    AllexpiresPage.prototype.sendpushnotification = function (item) {
        console.log(item);
        var url = 'https://zonepage.gr/gymapp/expirepushnotification.php';
        var postData = new FormData();
        postData.append('id', item.reg_id);
        postData.append('description', item.description);
        postData.append('expires', item.timehour1);
        postData.append('monthid', item.monthid);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    AllexpiresPage.prototype.updateprogram = function (item) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/updateuserprogram.php';
        var postData = new FormData();
        postData.append('prodid', item.progid);
        postData.append('userid', item.reg_id);
        postData.append('id', item.monthid);
        postData.append('expire', item.expires);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Add reminder		 
            if (result > 0) {
            }
            else {
                var alert_1 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    AllexpiresPage.prototype.gotoclient = function (item) {
        //console.log (item.idpro);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__customerview_customerview__["a" /* CustomerviewPage */], { item: item });
    };
    AllexpiresPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AllexpiresPage');
    };
    AllexpiresPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-allexpires',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/allexpires/allexpires.html"*/'<!--\n  Generated template for the AllexpiresPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Προγράμματα που έχουν λήξει</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n <h2>Πακέτα πελατών που λήγουν</h2> \n	<p>Στις επόμενες 30 ημέρες. Επιλέξτε πελάτη για να του στείλετε notification</p>\n	 <ion-list>\n\n <ion-item *ngFor="let item of expireusers" class="expires" (click)="gotoclient(item)" [ngClass]="{\'expire\' : (item.expires < nowdate)}">\n    <h3>{{item.fullname}}  {{item.lastname}}</h3> \n	<p>{{item.title}}  {{item.expires}}   -   Έχετε στείλει {{item.sendpushes}} ειδοποιήσεις</p>\n	\n	\n    </ion-item> \n\n\n\n</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/allexpires/allexpires.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], AllexpiresPage);
    return AllexpiresPage;
}());

//# sourceMappingURL=allexpires.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddtargetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the AddtargetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddtargetPage = /** @class */ (function () {
    function AddtargetPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.isenabled = false;
        this.datas = this.navParams.get('item');
        console.log(this.datas);
        this.getmetrics();
    }
    AddtargetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad AddtargetPage');
    };
    AddtargetPage.prototype.addmetrics = function () {
        console.log(this.toppings);
    };
    /** Send Target to Database */
    AddtargetPage.prototype.addmetric = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/addtarget.php';
        var postData = new FormData();
        postData.append('id', this.datas);
        postData.append('target_name', this.toppings);
        postData.append('target_metric', this.metric);
        this.dats = this.http.post(url, postData);
        this.dats.subscribe(function (data) {
            if (data == "error") {
            }
            console.log(data);
        });
    };
    AddtargetPage.prototype.getmetrics = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/getmetrics.php';
        var postData = new FormData();
        postData.append('id', this.datas);
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items = data;
            console.log(_this.items.length);
            if (_this.items.length > 0) {
                //enable the button
                _this.isenabled = true;
            }
            else {
                //disable the button
                _this.isenabled = false;
            }
        });
    };
    AddtargetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-addtarget',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/addtarget/addtarget.html"*/'<!--\n  Generated template for the AddtargetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n    <ion-navbar>\n        <ion-title>Προσθήκη Στόχου</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <ion-list>\n\n        <ion-item>\n            <ion-label>Επιλογή Στόχου</ion-label>\n            <ion-select [(ngModel)]="toppings" multiple="false" cancelText="Cancel" okText="Ok">\n                <ion-option value = "0" selected>Βάρος</ion-option>\n                <ion-option value = "2">Μυϊκή Μάζα</ion-option>\n                <ion-option value = "3">Σπλαχνικό Λίπος</ion-option>\n                <ion-option value = "4">Μεταβολική Ηλικία</ion-option>\n\n            </ion-select>\n        </ion-item>\n\n        <ion-item>\n            <ion-label color="primary" stacked>Στόχος που θέτει ο πελάτης</ion-label>\n            <ion-input type="text" placeholder="Kg" [(ngModel)]="metric"></ion-input>\n        </ion-item>\n\n\n    </ion-list>\n    <button ion-button block outline [disabled]="!isenabled" (click)="addmetric()">Προσθήκη</button>\n\n</ion-content>\n\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/addtarget/addtarget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], AddtargetPage);
    return AddtargetPage;
}());

//# sourceMappingURL=addtarget.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewtargetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ViewtargetPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewtargetPage = /** @class */ (function () {
    function ViewtargetPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ViewtargetPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ViewtargetPage');
    };
    ViewtargetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-viewtarget',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/viewtarget/viewtarget.html"*/'<!--\n  Generated template for the ViewtargetPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>viewtarget</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/viewtarget/viewtarget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ViewtargetPage);
    return ViewtargetPage;
}());

//# sourceMappingURL=viewtarget.js.map

/***/ }),

/***/ 512:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(513);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_module__ = __webpack_require__(517);



Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["enableProdMode"])();
Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 517:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_tooltips__ = __webpack_require__(842);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(845);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_home_home__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_trainerhome_trainerhome__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_customers_customers__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_addcustomer_addcustomer__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_customerview_customerview__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_list_list__ = __webpack_require__(846);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_barcode_scanner__ = __webpack_require__(847);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_weeklyprograms_weeklyprograms__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_addprogram_addprogram__ = __webpack_require__(139);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_addprogram_addprogram_module__ = __webpack_require__(335);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_editprogram_editprogram__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_classes_classes__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_editclass_editclass__ = __webpack_require__(175);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_addclass_addclass__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_orders_orders__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__ = __webpack_require__(506);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_splash_screen__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__providers_images_images__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_storage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_vieworder_vieworder__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_usermetrics_usermetrics__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_useraddmetric_useraddmetric__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_requestforuser_requestforuser__ = __webpack_require__(140);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_requestforuser_requestforuser_module__ = __webpack_require__(464);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_login_login__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_invoices_invoices__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_createinvoice_createinvoice__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_dateshistory_dateshistory__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_cutomersstats_cutomersstats__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_allexpires_allexpires__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_targets_targets__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_addtarget_addtarget__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_viewtarget_viewtarget__ = __webpack_require__(511);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40_ap_angular2_fullcalendar_src_calendar_calendar__ = __webpack_require__(848);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40_ap_angular2_fullcalendar_src_calendar_calendar___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_40_ap_angular2_fullcalendar_src_calendar_calendar__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_viewmetrics_viewmetrics__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_editcustomerprogram_editcustomerprogram__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_traines_priva_traines_priva__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_watersbuy_watersbuy__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__ionic_native_clipboard__ = __webpack_require__(850);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_sendzoom_sendzoom__ = __webpack_require__(142);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pages_sendzoom_sendzoom_module__ = __webpack_require__(465);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
















































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_trainerhome_trainerhome__["a" /* TrainerhomePage */],
                __WEBPACK_IMPORTED_MODULE_40_ap_angular2_fullcalendar_src_calendar_calendar__["CalendarComponent"],
                __WEBPACK_IMPORTED_MODULE_37__pages_targets_targets__["a" /* TargetsPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_addtarget_addtarget__["a" /* AddtargetPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_viewtarget_viewtarget__["a" /* ViewtargetPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_customers_customers__["a" /* CustomersPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_customerview_customerview__["a" /* CustomerviewPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_addcustomer_addcustomer__["a" /* AddcustomerPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_viewmetrics_viewmetrics__["a" /* ViewmetricsPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_editprogram_editprogram__["a" /* EditprogramPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_classes_classes__["a" /* ClassesPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_editclass_editclass__["a" /* EditclassPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_addclass_addclass__["a" /* AddclassPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_orders_orders__["a" /* OrdersPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_vieworder_vieworder__["a" /* VieworderPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_usermetrics_usermetrics__["a" /* UsermetricsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_useraddmetric_useraddmetric__["a" /* UseraddmetricPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_editcustomerprogram_editcustomerprogram__["a" /* EditcustomerprogramPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_traines_priva_traines_priva__["a" /* TrainesPrivaPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_invoices_invoices__["a" /* InvoicesPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_createinvoice_createinvoice__["a" /* CreateinvoicePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_dateshistory_dateshistory__["a" /* DateshistoryPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_cutomersstats_cutomersstats__["a" /* CutomersstatsPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_allexpires_allexpires__["a" /* AllexpiresPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_watersbuy_watersbuy__["a" /* WatersbuyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/addclass/addclass.module#AddclassPageModule', name: 'AddclassPage', segment: 'addclass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addcustomer/addcustomer.module#AddcustomerPageModule', name: 'AddcustomerPage', segment: 'addcustomer', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addproduct/addproduct.module#AddproductPageModule', name: 'AddproductPage', segment: 'addproduct', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addprogram/addprogram.module#AddprogramPageModule', name: 'something-els', segment: 'some-othr-path/:date', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addtarget/addtarget.module#AddtargetPageModule', name: 'AddtargetPage', segment: 'addtarget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/classes/classes.module#ClassesPageModule', name: 'ClassesPage', segment: 'classes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/createinvoice/createinvoice.module#CreateinvoicePageModule', name: 'CreateinvoicePage', segment: 'createinvoice', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customers/customers.module#CustomersPageModule', name: 'CustomersPage', segment: 'customers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cutomersstats/cutomersstats.module#CutomersstatsPageModule', name: 'CutomersstatsPage', segment: 'cutomersstats', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/dateshistory/dateshistory.module#DateshistoryPageModule', name: 'DateshistoryPage', segment: 'dateshistory', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editclass/editclass.module#EditclassPageModule', name: 'EditclassPage', segment: 'editclass', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editcustomerprogram/editcustomerprogram.module#EditcustomerprogramPageModule', name: 'EditcustomerprogramPage', segment: 'editcustomerprogram', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editproduct/editproduct.module#EditproductPageModule', name: 'EditproductPage', segment: 'editproduct', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editprogram/editprogram.module#EditprogramPageModule', name: 'EditprogramPage', segment: 'editprogram', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/hometrainers/hometrainers.module#HometrainersPageModule', name: 'HometrainersPage', segment: 'hometrainers', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/invoices/invoices.module#InvoicesPageModule', name: 'InvoicesPage', segment: 'invoices', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/orders/orders.module#OrdersPageModule', name: 'OrdersPage', segment: 'orders', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/products/products.module#ProductsPageModule', name: 'ProductsPage', segment: 'products', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/requestforuser/requestforuser.module#RequestforuserPageModule', name: 'something-else', segment: 'some-other-path/:progid', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sendzoom/sendzoom.module#SendzoomPageModule', name: 'SendzoomPage', segment: 'sendzoom', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/trainerhome/trainerhome.module#TrainerhomePageModule', name: 'TrainerhomePage', segment: 'trainerhome', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/targets/targets.module#TargetsPageModule', name: 'TargetsPage', segment: 'targets', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/traines-priva/traines-priva.module#TrainesPrivaPageModule', name: 'TrainesPrivaPage', segment: 'traines-priva', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/useraddmetric/useraddmetric.module#UseraddmetricPageModule', name: 'UseraddmetricPage', segment: 'useraddmetric', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/vieworder/vieworder.module#VieworderPageModule', name: 'VieworderPage', segment: 'vieworder', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewmetrics/viewmetrics.module#ViewmetricsPageModule', name: 'ViewmetricsPage', segment: 'viewmetrics', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/usermetrics/usermetrics.module#UsermetricsPageModule', name: 'UsermetricsPage', segment: 'usermetrics', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/viewtarget/viewtarget.module#ViewtargetPageModule', name: 'ViewtargetPage', segment: 'viewtarget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/weeklyprograms/weeklyprograms.module#WeeklyprogramsPageModule', name: 'WeeklyprogramsPage', segment: 'weeklyprograms', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/watersbuy/watersbuy.module#WatersbuyPageModule', name: 'WatersbuyPage', segment: 'watersbuy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/customerview/customerview.module#CustomerviewPageModule', name: 'CustomerviewPage', segment: 'customerview', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_24__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_22__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_29__pages_requestforuser_requestforuser_module__["RequestforuserPageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_addprogram_addprogram_module__["AddprogramPageModule"],
                __WEBPACK_IMPORTED_MODULE_3_ionic_tooltips__["a" /* TooltipsModule */],
                __WEBPACK_IMPORTED_MODULE_47__pages_sendzoom_sendzoom_module__["SendzoomPageModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_5__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_40_ap_angular2_fullcalendar_src_calendar_calendar__["CalendarComponent"],
                __WEBPACK_IMPORTED_MODULE_10__pages_list_list__["a" /* ListPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_customers_customers__["a" /* CustomersPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_customerview_customerview__["a" /* CustomerviewPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_addtarget_addtarget__["a" /* AddtargetPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_targets_targets__["a" /* TargetsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_addcustomer_addcustomer__["a" /* AddcustomerPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_addprogram_addprogram__["a" /* AddprogramPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_editprogram_editprogram__["a" /* EditprogramPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_classes_classes__["a" /* ClassesPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_traines_priva_traines_priva__["a" /* TrainesPrivaPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_editclass_editclass__["a" /* EditclassPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_addclass_addclass__["a" /* AddclassPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_trainerhome_trainerhome__["a" /* TrainerhomePage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_orders_orders__["a" /* OrdersPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_vieworder_vieworder__["a" /* VieworderPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_usermetrics_usermetrics__["a" /* UsermetricsPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_useraddmetric_useraddmetric__["a" /* UseraddmetricPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_requestforuser_requestforuser__["a" /* RequestforuserPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_sendzoom_sendzoom__["a" /* SendzoomPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_invoices_invoices__["a" /* InvoicesPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_createinvoice_createinvoice__["a" /* CreateinvoicePage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_dateshistory_dateshistory__["a" /* DateshistoryPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_cutomersstats_cutomersstats__["a" /* CutomersstatsPage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_allexpires_allexpires__["a" /* AllexpiresPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_sendzoom_sendzoom__["a" /* SendzoomPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_viewmetrics_viewmetrics__["a" /* ViewmetricsPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_editcustomerprogram_editcustomerprogram__["a" /* EditcustomerprogramPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_watersbuy_watersbuy__["a" /* WatersbuyPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_barcode_scanner__["a" /* BarcodeScanner */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_23__providers_images_images__["a" /* ImagesProvider */],
                __WEBPACK_IMPORTED_MODULE_32__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_40_ap_angular2_fullcalendar_src_calendar_calendar__["CalendarComponent"],
                __WEBPACK_IMPORTED_MODULE_45__ionic_native_clipboard__["a" /* Clipboard */],
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 68:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__addcustomer_addcustomer__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__customerview_customerview__ = __webpack_require__(69);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the CustomersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CustomersPage = /** @class */ (function () {
    function CustomersPage(navCtrl, navParams, http, iab) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.iab = iab;
        this.loadData('1');
    }
    CustomersPage.prototype.loadData = function (value) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/getcustomers.php';
        var postData = new FormData();
        postData.append('category', value);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.items = result;
            console.log(result.length);
            _this.total = result.length;
        });
    };
    CustomersPage.prototype.gotoproduct = function (item) {
        //console.log (item.idpro);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__customerview_customerview__["a" /* CustomerviewPage */], { item: item });
    };
    CustomersPage.prototype.addcustomer = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__addcustomer_addcustomer__["a" /* AddcustomerPage */]);
    };
    CustomersPage.prototype.onChange = function (value) {
        console.log(value);
        this.programfilter = value;
        this.loadData(this.programfilter);
    };
    CustomersPage.prototype.getItems = function (ev) {
        // Reset items back to all of the items
        //this.loadData();
        // set val to the value of the ev target
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.items = this.items.filter(function (item) {
                var usertext;
                usertext = item.lastname + item.fullname + item.email + item.mobilephone;
                return (usertext.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
        else {
            this.loadData(this.programfilter);
        }
    };
    CustomersPage.prototype.openWebpage = function (item) {
        var num = item.path;
        var url = "https://zonepage.gr/gymapp/appcrm/invoices/" + num;
        // Opening a URL and returning an InAppBrowserObject
        var browser = this.iab.create(url, '_blank');
        // Inject scripts, css and more with browser.X
    };
    CustomersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CustomersPage');
    };
    CustomersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customers',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/customers/customers.html"*/'<!--\n  Generated template for the CustomersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>Πελάτες</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n<ion-grid>\n<ion-row>\n<ion-col col-lg-2>\n<h2>Συνολο Πελατών</h2>\n<h2>{{total}}</h2>\n\n\n<button ion-button block (click)="addcustomer()">Προσθηκη Πελατη</button>\n\n <ion-item class="filt">\n    <ion-label>Filter</ion-label>\n    <ion-select [(ngModel)]="toppings" multiple="false" cancelText="Cancel" okText="Ok" (ionChange)="onChange($event)">\n      <ion-option value = "1" selected>Ενεργοί Πελάτες</ion-option>\n      <ion-option value = "2">App Χρήστες</ion-option>\n      <ion-option value = "4">Ανενεργοί Χρήστες/Demo</ion-option>\n      <ion-option value = "3">Όλοι οι Χρήστες</ion-option>\n\n     \n    </ion-select>\n  </ion-item>\n	  </ion-col>\n	  <ion-col col-lg-10>\n	  <p>Διαγράψτε την αναζήτηση για να εκτελέσετε νέα ή να επαναφέρετε όλους τους πελάτες</p>\n	   <ion-searchbar placeholder="Αναζήτηση με Επίθετο, Όνομα, email ή κινητό" (ionInput)="getItems($event)"></ion-searchbar>\n<ion-grid class="headrow">\n  <ion-row>\n    <ion-col col-1>Id\n	  </ion-col>\n  <ion-col col-2>Ονοματεπώνυμο\n	  </ion-col>\n\n  \n	<ion-col col-2>Κινητό Τηλέφωνο</ion-col>\n	<ion-col col-2>Credits</ion-col>\n	<ion-col col-2>Total Credits</ion-col>\n  <ion-col col-2>Invoice</ion-col>\n  </ion-row>\n</ion-grid>\n<ion-list>\n    <ion-item *ngFor="let item of items" (click)="gotoproduct(item)">\n    \n	<ion-grid>\n  <ion-row>\n   <ion-col col-lg-1>{{item.reg_id}}</ion-col>\n    <ion-col col-lg-2><h2>{{item.fullname}}  {{item.lastname}}</h2>\n	  </ion-col>\n\n   \n	<ion-col col-lg-2>{{item.mobilephone}}</ion-col>\n	<ion-col col-lg-2>{{item.Points}} Credits</ion-col>\n		<ion-col col-lg-2>{{item.totalpoints}} Credits</ion-col>\n      <ion-col col-lg-2><button *ngIf="item.path"  ion-button block clear (click)="openWebpage(item)">Open invoice</button>\n          <button *ngIf="!item.path"  ion-button block clear >-</button>\n      </ion-col>\n  </ion-row>\n</ion-grid>\n	\n	\n      \n     \n    </ion-item>\n  </ion-list>\n\n  </ion-col>\n  \n    </ion-row>\n</ion-grid>\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/customers/customers.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]])
    ], CustomersPage);
    return CustomersPage;
}());

//# sourceMappingURL=customers.js.map

/***/ }),

/***/ 69:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CustomerviewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__usermetrics_usermetrics__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__customers_customers__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__targets_targets__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__editcustomerprogram_editcustomerprogram__ = __webpack_require__(172);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the CustomerviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CustomerviewPage = /** @class */ (function () {
    function CustomerviewPage(navCtrl, navParams, http, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.comming = [];
        this.userdata = [];
        this.nowdate = new Date().toISOString();
        this.datas = this.navParams.get('item');
        this.loadData();
        this.userprograms();
        this.getusercome();
        this.getusermessge();
        this.customid = 0;
        this.customcredits = 0;
        this.countprogr(0, 0);
    }
    CustomerviewPage_1 = CustomerviewPage;
    CustomerviewPage.prototype.loadData = function () {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/getuser.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.userdata = result[0];
            _this.firstname = _this.userdata.fullname;
            _this.lastname = _this.userdata.lastname;
            _this.phone = _this.userdata.phone;
            _this.birthdate = _this.userdata.birthdate;
            _this.mobilephone = _this.userdata.mobilephone;
            _this.email = _this.userdata.email;
            _this.adress = _this.userdata.adress;
            _this.credits = _this.userdata.Points;
            _this.pass = _this.userdata.password;
            _this.gender = _this.userdata.gender;
            _this.height = _this.userdata.height;
            _this.money = _this.userdata.money;
            _this.weight = _this.userdata.weight;
            _this.pathiseis = _this.userdata.pathises;
            //this.storage.set('id', data);
            if (_this.userdata.enable == '1') {
                _this.entry = true;
            }
            if (_this.userdata.enable == '0') {
                _this.entry = false;
            }
        });
    };
    CustomerviewPage.prototype.userprograms = function () {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/getusersprograms.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.userprogram = result;
            //this.storage.set('id', data);	
            console.log(_this.userprogram);
        });
    };
    CustomerviewPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CustomerviewPage');
    };
    CustomerviewPage.prototype.goupdatecredits = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/updatecustomercredits.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        postData.append('credits', this.credits);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__customers_customers__["a" /* CustomersPage */]);
    };
    CustomerviewPage.prototype.goupdatemoney = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/updatecustomermoney.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        postData.append('credits', this.money);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__customers_customers__["a" /* CustomersPage */]);
    };
    CustomerviewPage.prototype.gologin = function () {
        var url = 'https://zonepage.gr/gymapp/appcrm/addprogramtocustomer.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        postData.append('yoga', this.yoga);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__customers_customers__["a" /* CustomersPage */]);
    };
    CustomerviewPage.prototype.metrics = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__usermetrics_usermetrics__["a" /* UsermetricsPage */], { item: this.datas.reg_id });
    };
    CustomerviewPage.prototype.gotargets = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__targets_targets__["a" /* TargetsPage */], { item: this.datas.reg_id });
    };
    CustomerviewPage.prototype.goupdate = function () {
        console.log(this.gender);
        var enable;
        console.log(this.entry);
        if (this.entry == false) {
            enable = '0';
        }
        if (this.entry == true) {
            enable = '1';
        }
        var url = 'https://zonepage.gr/gymapp/appcrm/updatecustomer.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        postData.append('firstname', this.firstname);
        postData.append('lastname', this.lastname);
        postData.append('adresss', this.adress);
        postData.append('phone', this.phone);
        postData.append('mobilephone', this.mobilephone);
        postData.append('datebirth', this.birthdate);
        postData.append('email', this.email);
        postData.append('password', this.pass);
        postData.append('enable', enable);
        postData.append('gender', this.gender);
        postData.append('height', this.height);
        postData.append('weight', this.weight);
        postData.append('pathiseis', this.pathiseis);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            console.log(data);
            if (data == "error") {
            }
        });
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__customers_customers__["a" /* CustomersPage */]);
    };
    CustomerviewPage.prototype.resentemail = function () {
        var url = 'https://mendez.gr/crm/newuseremail.php';
        var postData = new FormData();
        postData.append('name', this.firstname);
        postData.append('email', this.email);
        postData.append('pass', this.pass);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (dataa) {
            if (dataa == "error") {
            }
            else {
            }
        });
    };
    CustomerviewPage.prototype.gotoproduct = function (item, item1, item2, item3, item4) {
        var _this = this;
        var url = 'https://zonepage.gr/gymapp/neworder.php';
        var postData = new FormData();
        postData.append('product', item);
        postData.append('customer', this.datas.reg_id);
        postData.append('amount', item1);
        postData.append('credits', this.customcredits);
        postData.append('gateaway', '0');
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
            else {
                console.log(data);
                var alert_1 = _this.alertCtrl.create({
                    title: 'Ολοκλήρωση',
                    subTitle: 'To Προϊόν Προστέθηκε στις παραγγελίες. Μεταβείτε στα Ordres για επιβεβαίωση',
                    buttons: ['OK']
                });
                alert_1.present();
            }
        });
    };
    CustomerviewPage.prototype.getusercome = function () {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/historycominguser2.php', postData);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    CustomerviewPage.prototype.deleteprogram = function (item) {
        var _this = this;
        var name = item.lastname;
        var confirm = this.alertCtrl.create({
            title: 'Θέλετε να ακυρώσετε το ραντεβού για ' + name,
            message: 'Επιλέγοντα "ΝΑΙ" επιστρέφονται στον πελάτη τα μισά Credits. Επιλέγοντας ναι με επιστροφή Credits επιστρέφονται όλα τα Credits',
            buttons: [
                { text: 'NO',
                    handler: function () {
                    } },
                { text: 'ΝΑΙ τα μισά',
                    handler: function () {
                        _this.returncredits = (parseInt(item.costcredits) / 2).toFixed(0);
                        _this.deleteregister(item, _this.returncredits);
                        console.log(_this.returncredits);
                    } },
                { text: 'ΝΑΙ Επιστροφή Credits',
                    handler: function () {
                        _this.deleteregister(item, item.costcredits);
                    } },
                { text: 'Να μην Επιστραφούν',
                    handler: function () {
                        _this.deleteregister(item, '0');
                    } }
            ]
        });
        confirm.present();
    };
    CustomerviewPage.prototype.deleteregister = function (item, returnc) {
        var _this = this;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/deleteregister.php';
        var postData = new FormData();
        postData.append('prodid', item.progid);
        postData.append('userid', item.reg_id);
        postData.append('return', returnc);
        postData.append('id', item.cpid);
        postData.append('person_id', item.person_id);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            //Add reminder		 
            if (result == 'Registration Complete') {
                _this.navCtrl.setRoot(CustomerviewPage_1);
            }
            else {
                var alert_2 = _this.alertCtrl.create({
                    title: "There is a Problem",
                    //subTitle: result,
                    buttons: ['OK']
                });
                alert_2.present();
            }
        });
    };
    CustomerviewPage.prototype.selecttime = function (range, cost, months) {
        this.custommonths = months;
        this.customecost = cost;
        this.customid = range;
    };
    CustomerviewPage.prototype.calculatecost = function () {
        this.finalcosts = this.customecost * this.customcredits;
    };
    CustomerviewPage.prototype.gotocurrentpro = function (item) {
        //console.log (item.idpro);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__editcustomerprogram_editcustomerprogram__["a" /* EditcustomerprogramPage */], { item: item, person: this.datas });
    };
    CustomerviewPage.prototype.sendmesage = function () {
        var _this = this;
        if (this.messagetext && this.nottitle) {
            var url = 'https://zonepage.gr/gymapp/appcrm/sendnotigication.php';
            var postData = new FormData();
            postData.append('id', this.datas.reg_id);
            postData.append('description', this.messagetext);
            postData.append('title', this.nottitle);
            this.data = this.http.post(url, postData);
            this.data.subscribe(function (dataa) {
                console.log(dataa);
                if (dataa == "success") {
                    var alert_3 = _this.alertCtrl.create({
                        title: "Στάλθηκε με επιτυχία",
                        //subTitle: result,
                        buttons: ['OK']
                    });
                    alert_3.present();
                    _this.messagetext = '';
                    _this.nottitle = '';
                    _this.getusermessge();
                }
                else {
                }
            });
        }
        else {
            var alert_4 = this.alertCtrl.create({
                title: "Συμπληρώστε όλα τα πεδία",
                //subTitle: result,
                buttons: ['OK']
            });
            alert_4.present();
        }
    };
    CustomerviewPage.prototype.getusermessge = function () {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        data = this.http.post('https://zonepage.gr/gymapp/appcrm/getusermessages.php', postData);
        data.subscribe(function (result) {
            _this.allmessages = result;
            console.log(_this.allmessages);
        });
    };
    CustomerviewPage.prototype.countprogr = function ($month, $year) {
        var _this = this;
        this.count = 0;
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/commingrule.php';
        var postData = new FormData();
        postData.append('id', this.datas.reg_id);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.count = result.length;
            console.log(result);
        });
        console.log(this.count);
    };
    CustomerviewPage = CustomerviewPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-customerview',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/customerview/customerview.html"*/'<!--\n  Generated template for the CustomerviewPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Πελάτης: {{userdata.fullname}} {{userdata.lastname}}\n</ion-title>\n<ion-buttons end>\n<button ion-button primary  end (click)="gotargets()">Στόχοι</button>\n<button ion-button primary  end (click)="metrics()">Metrics</button>\n<button ion-button primary  end (click)="goupdate()">Update</button>\n</ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding> \n\n<ion-grid class="headrow">\n  <ion-row>\n    <ion-col col-lg-3><h3 class="dividertitle">Προσωπικά Στοιχεία</h3>\n      <ion-list>\n        <ion-item>\n          <ion-label color="primary" stacked>\'Ονομα</ion-label>\n          <ion-input type="text" placeholder="Όνομα" [(ngModel)]="firstname"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Επίθετο</ion-label>\n          <ion-input type="text" placeholder="Επίθετο" [(ngModel)]="lastname"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Ημερομηνία Γέννησης</ion-label>\n          <ion-datetime displayFormat="MM/DD/YYYY" [(ngModel)]="birthdate"></ion-datetime>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Σταθερο Τηλέφωνο</ion-label>\n          <ion-input type="number" placeholder="00000" [(ngModel)]="phone"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Κινητό Τηλέφωνο</ion-label>\n          <ion-input type="number" placeholder="00000" [(ngModel)]="mobilephone"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Email</ion-label>\n          <ion-input type="email" placeholder="email@emeil.com" [(ngModel)]="email"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Password</ion-label>\n          <ion-input type="text" placeholder="***" [(ngModel)]="pass"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Διεύθυνση Κατοικίας</ion-label>\n          <ion-input type="Text" placeholder="Κατοικία" [(ngModel)]="adress"></ion-input>\n        </ion-item>\n      </ion-list>\n\n    </ion-col>\n    <ion-col col-lg-3><h3 class="dividertitle">Ιατρικά Στοιχεία</h3>\n      <ion-list>\n        <ion-item>\n          <ion-label>Gender</ion-label>\n          <ion-select [(ngModel)]="gender" >\n            <ion-option value="f">Γυναίκα</ion-option>\n            <ion-option value="m">Άντρας</ion-option>\n          </ion-select>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked >Ύψος (m)</ion-label>\n          <ion-input type="number" placeholder="m" [(ngModel)]="height"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Βάρος)</ion-label>\n          <ion-input type="number" placeholder="Kg" [(ngModel)]="weight"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Παθήσεις</ion-label>\n          <ion-textarea [(ngModel)]="pathiseis"></ion-textarea>\n        </ion-item>\n        <ion-item>\n          <ion-label color="primary" stacked>Ενεργός Χρήστης</ion-label>\n          <ion-checkbox slot="end" [(ngModel)]="entry"></ion-checkbox>\n        </ion-item>\n        <ion-item>\n          <button ion-button (click)="resentemail()">Αποστολή Login Email</button>\n        </ion-item>\n      </ion-list>\n    </ion-col>\n    <ion-col col-lg-3>\n	     <h3 class="dividertitle">Ραντεβού Χρήστη</h3>\n       <p>Μπορείτε να ακυρώσετε συγκεκριμένο ραντεβού με Click</p>\n        <ion-list class="rantevou">\n\n          <ion-item *ngFor="let item of users" (click)="deleteprogram(item)">\n            <h3>{{item.fullname}}  {{item.lastname}}</h3> \n            <p>{{item.tilte}}</p>\n            <p>{{item.timehour1}}</p>\n          </ion-item> \n        </ion-list>\n\n        <h3 class="dividertitle" >Personal Μήνα : {{count}} / 12</h3>\n\n      </ion-col>\n	<ion-col col-lg-3>\n    <h3 class="dividertitle">Credits</h3>\n    <h3>{{userdata.Points}}</h3>\n    <ion-label color="primary" stacked>Edit Credits</ion-label>\n    <ion-input type="text" placeholder="Όνομα" [(ngModel)]="credits"></ion-input>\n    <button ion-button (click)="goupdatecredits()">Update Credits</button>\n\n   <h3 class="dividertitle">Money</h3>\n    <h3>{{userdata.money}} €</h3>\n    <ion-label color="primary" stacked>Edit Money</ion-label>\n    <ion-input type="text" placeholder="Money" [(ngModel)]="money"></ion-input>\n    <button ion-button (click)="goupdatemoney()">Update Money</button>\n\n    <h3 class="dividertitle">Προγράμματα</h3>\n	\n    <ion-list>\n      <ion-item *ngFor="let item of userprogram" (click)="gotocurrentpro(item)" [ngClass]="{\'expire\' : (item.expires < nowdate)}">\n       \n    \n	<ion-grid>\n  <ion-row>\n    <ion-col col-8><h2>{{item.title}}</h2>\n\n        <p *ngIf="item.getcredits > 0">{{ item.getcredits }} Credits from {{item.sender}}</p>  \n        <p *ngIf="item.renew == 1">Έχει μπει στο αρχείο</p>  \n	  <p>Add - {{item.create}}</p>\n      <p>Expires - {{item.expires}}</p>\n	 </ion-col>\n\n    \n  </ion-row>\n</ion-grid>\n	\n	\n      \n     \n    </ion-item>\n  </ion-list>\n	\n	\n	\n\n	\n	</ion-col>\n\n  </ion-row>\n</ion-grid>\n\n\n\n<ion-grid class="headrow">\n  <ion-row>\n   <ion-col col-lg-12><h3 class="dividertitle">Message Notifications</h3></ion-col>\n         <ion-grid>\n                <ion-row>\n                    <ion-col col-lg-3>\n                        <h3 class="dividertitle">New Notifications</h3>\n                            <ion-input class="intitle" type="text" placeholder="Title" [(ngModel)]="nottitle"></ion-input>\n                            <ion-textarea  class="intitle" rows="10" placeholder="Message" [(ngModel)]="messagetext"></ion-textarea>\n                            <button ion-button (click)="sendmesage()">Send Message</button>\n                        </ion-col>\n                        <ion-col col-lg-7>\n                            <h3 class="dividertitle">User Notifications</h3>\n                                  <ion-list class="rantevou">\n                                   <ion-item *ngFor="let item of allmessages">\n         \n                                         <ion-grid>\n                                            <ion-row>\n                                                 <ion-col col-8><h2>{{item.headings}}</h2>      \n      <p>{{item.send_date}}</p>\n      <p>{{item.content}}</p>\n     </ion-col>\n\n    \n  </ion-row>\n</ion-grid>\n    \n    \n      \n     \n    </ion-item>\n  </ion-list>\n\n\n\n\n                        </ion-col>\n                    </ion-row>\n                </ion-grid>\n\n\n\n </ion-row>\n</ion-grid> \n\n\n<!-- Προγράμματα Custom -->\n\n<ion-grid class="headrow">\n    <ion-row>\n        <ion-col col-lg-12><h3 class="dividertitle">Προσθήκη Custom Προγράμματος σε Χρήστη</h3>\n\n            <ion-grid>\n                <ion-row>\n                    <ion-col col-lg-3>\n                        <h5>Επιλέξτε Διάρκεια</h5>\n                        <ion-list>\n                            <button ion-item (click)="selecttime(10000, 0.8,\'1 μήνας\' )">\n                                <ion-grid>\n                                    <ion-row>\n                                        <ion-col col-8>\n                                            <h2>1 μήνας</h2>\n                                        </ion-col>\n\n                                        <ion-col col-4>\n                                            <span class="price">0,8€/Credit</span>\n                                        </ion-col>\n                                    </ion-row>\n                                </ion-grid>     \n                            </button>\n\n\n\n                        <button ion-item (click)="selecttime(10001, 0.7,\'3 μήνες\' )">\n\n                        <ion-grid>\n                        <ion-row>\n                        <ion-col col-8><h2>3 μήνες</h2></ion-col>\n                        <ion-col col-4><span class="price">0,7€/Credit</span></ion-col>\n                        </ion-row>\n                        </ion-grid>     \n\n                        </button>\n\n\n\n                        <button ion-item (click)="selecttime(10002, 0.6,\'6 μήνες\' )">\n\n                        <ion-grid>\n                        <ion-row>\n                        <ion-col col-8><h2>6 μήνες</h2>\n                       </ion-col>\n\n                        <ion-col col-4><span class="price">0,6€/Credit</span></ion-col>\n                        </ion-row>\n                        </ion-grid>     \n\n                        </button>\n                        </ion-list>\n                    </ion-col>\n                    <ion-col col-lg-3>\n                        <h5>Εισάγετε Συνολικά Credits</h5>\n                        <ion-list padding>\n                            <ion-item no-padding>\n                                <h5>{{customecost}}€/credit</h5>\n                                <h5>{{custommonths}}</h5>\n                                \n                            </ion-item>\n                            <ion-item no-padding>\n                                <ion-input type="number" [(ngModel)]="customcredits" value="80"></ion-input>\n                         \n                            </ion-item>\n                             <ion-item no-padding>\n                                \n                                <button ion-button (click)="calculatecost()">Update</button>\n                            </ion-item>\n                           \n                        </ion-list>\n                    </ion-col>\n\n\n                    <ion-col col-lg-3>\n                        <ion-list padding>\n                            <ion-item no-padding>\n                                 <h5>Συνολικό Κόστος</h5>\n                                 <h2 text-center>{{finalcosts}}</h2>\n                            </ion-item>\n                             <ion-item no-padding>\n                          \n                                <button ion-button (click)="gotoproduct(customid, finalcosts,\'Custom\',\'1 μήνες\', customcredits )">Buy Program</button>\n                            </ion-item>\n                            </ion-list>\n                            \n\n                       \n                    </ion-col>\n\n                    <ion-col col-lg-3>\n                       \n                    </ion-col>\n\n                </ion-row>\n            </ion-grid>\n\n        </ion-col>\n\n    </ion-row>\n</ion-grid>\n\n<!-- Προγράμματα Απεριόριστα -->\n\n<ion-grid class="headrow">\n    <ion-row>\n        <ion-col col-lg-12><h3 class="dividertitle">Προσθήκη Προγράμματος σε Χρήστη</h3>\n\n            <ion-grid>\n                <ion-row>\n                    <ion-col col-lg-3>\n                        <h5>Pilate Tower</h5>\n                        <ion-list>\n                            <button ion-item (click)="gotoproduct(6,\'120\',\'Medium\',\'1 μήνες\' )">\n                                <ion-grid>\n                                    <ion-row>\n                                        <ion-col col-8>\n                                            <h2>Pilate Tower 1 μήνας</h2>\n                                            <p>150 Credits</p>\n                                            <p>Διάρκεια 1 μήνες</p>\n                                            <p>Απεριόριστα Pilates</p>\n                                        </ion-col>\n\n                                        <ion-col col-4>\n                                            <span class="price">120€</span>\n                                        </ion-col>\n                                    </ion-row>\n                                </ion-grid>     \n                            </button>\n\n\n\n                        <button ion-item (click)="gotoproduct(7,\'180\',\'Medium\',\'2 μήνες\' )">\n\n                        <ion-grid>\n                        <ion-row>\n                        <ion-col col-8><h2>Προσφορά 2</h2>\n                        <p>300 Credits</p>\n                        <p>Διάρκεια 2 μήνες</p>\n                        <p>Όλα τα προγράμματα</p></ion-col>\n\n                        <ion-col col-4><span class="price">180€</span></ion-col>\n                        </ion-row>\n                        </ion-grid>     \n\n                        </button>\n\n\n\n                        <button ion-item (click)="gotoproduct(0,\'260\',\'Medium\',\'3 μήνες\' )">\n\n                        <ion-grid>\n                        <ion-row>\n                        <ion-col col-8><h2>Προσφορά 3</h2>\n                        <p>400 Credits</p>\n                        <p>Διάρκεια 3 μήνες</p>\n                        <p>Όλα τα προγράμματα</p></ion-col>\n\n                        <ion-col col-4><span class="price">260€</span></ion-col>\n                        </ion-row>\n                        </ion-grid>     \n\n                        </button>\n                        </ion-list>\n                    </ion-col>\n                    <ion-col col-lg-3>\n                        <h5>Pilates</h5>\n                        <ion-list padding>\n                            <button ion-item no-padding (click)="gotoproduct(21,\'150\',\'Pilates\',\'1 μήνας\')">\n                                <div class="Yoga">\n                                    <ion-row>\n                                        <ion-col col-8>\n                                            <h2>Pilates</h2>\n                                            <p>1 μήνας</p>\n                                            <p>Απεριόριστες επισκέψεις PIlates Mat/Tower</p>\n                                        </ion-col>\n                                        <ion-col col-4>\n                                            <span class="price">150€</span>\n                                        </ion-col>\n                                    </ion-row>\n                                </div>\n                            </button >\n                            <button ion-item no-padding (click)="gotoproduct(22,\'360\',\'Pilates\',\'3 μήνες\' )">\n                                <div class="Yoga">\n                                    <ion-row>\n                                        <ion-col col-8>\n                                        <h2>Pilates</h2>\n                                        <p>3 μήνες</p>\n                                        <p>Απεριόριστες επισκέψεις PIlates Mat/Tower</p>\n                                        </ion-col>\n                                        <ion-col col-4><span class="price">360€</span></ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n\n                            <button ion-item no-padding (click)="gotoproduct(23,\'630\',\'Pilates\',\'6 μήνες\' )">\n                                <div class="Yoga">\n                                    <ion-row>\n                                        <ion-col col-8>\n                                        <h2>Pilates</h2>\n                                        <p>6 μήνες</p>\n                                        <p>Απεριόριστες επισκέψεις PIlates Mat/Tower</p>\n                                        </ion-col>\n                                        <ion-col col-4><span class="price">630€</span></ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n                            <button ion-item no-padding (click)="gotoproduct(24,\'1080\',\'Pilates\',\'12 μήνες\' )">\n                                <div class="Yoga">\n                                    <ion-row>\n                                        <ion-col col-8>\n                                        <h2>Pilates</h2>\n                                        <p>12 μήνες</p>\n                                        <p>Απεριόριστες επισκέψεις PIlates Mat/Tower</p>\n                                        </ion-col>\n                                        <ion-col col-4><span class="price">1080€</span></ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n                        </ion-list>\n                    </ion-col>\n\n\n                    <ion-col col-lg-3>\n                        <h5>Yoga</h5>\n                        <ion-list padding>\n                            <button ion-item no-padding (click)="gotoproduct(41,\'70\',\'Yoga\',\'1 μήνας\' )">\n                                <div class="Yoga">\n                                    <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Yoga</h2>\n                                        <p>1 μήνας</p>\n                                        <p>Απεριόριστες επισκέψεις</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">70€</span>\n                                    </ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n                            <button ion-item no-padding (click)="gotoproduct(42,\'168\',\'Yoga\',\'3 μήνες\')">\n                                <div class="Yoga">\n                                    <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Yoga</h2>\n                                        <p>3 μήνες</p>\n                                        <p>Απεριόριστες επισκέψεις</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">168€</span>\n                                    </ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n                            <button ion-item no-padding (click)="gotoproduct(43,\'294\',\'Yoga\',\'6 μήνες\')">\n                                <div class="Yoga">\n                                    <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Yoga</h2>\n                                        <p>6 μήνες</p>\n                                        <p>Απεριόριστες επισκέψεις</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">294€</span>\n                                    </ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n                            <button ion-item no-padding (click)="gotoproduct(44,\'504\',\'Yoga\',\'12 μήνες\')">\n                                <div class="Yoga">\n                                    <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Yoga</h2>\n                                        <p>12 μήνες</p>\n                                        <p>Απεριόριστες επισκέψεις</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">504€</span>\n                                    </ion-col>\n                                    </ion-row>\n                                </div>\n                            </button>\n\n\n\n                        </ion-list>\n                    </ion-col>\n\n                    <ion-col col-lg-3>\n                        <h5>Personal Training</h5>\n                            <ion-list padding>\n                                <button ion-item no-padding (click)="gotoproduct(31,\'120\',\'Presonal Trainning\',\'1 month\' )">\n                                    <div class="Yoga">\n                                        <ion-row>\n                                            <ion-col col-8>\n                                                <h2>Personal Training</h2>\n                                                <p>1 μήνας</p>\n                                                <p>Απεριόριστες επισκέψεις</p>\n                                            </ion-col>\n                                            <ion-col col-4>\n                                                <span class="price">120€</span>\n                                            </ion-col>\n                                        </ion-row>\n                                    </div>\n                                </button >\n                                <button ion-item no-padding (click)="gotoproduct(32,\'288\',\'Presonal Trainning\',\'3 month\' )">\n                                    <div class="Yoga">\n                                        <ion-row>\n                                            <ion-col col-8>\n                                                <h2>Personal Training</h2>\n                                                <p>3 μήνες</p>\n                                                <p>Απεριόριστες επισκέψεις</p>\n                                            </ion-col>\n                                            <ion-col col-4>\n                                                <span class="price">288€</span>\n                                            </ion-col>\n                                        </ion-row>\n                                    </div>\n                                </button >\n                                <button ion-item no-padding (click)="gotoproduct(33,\'504\',\'Presonal Trainning\',\'6 month\' )">\n                                    <div class="Yoga">\n                                        <ion-row>\n                                            <ion-col col-8>\n                                                <h2>Personal Training</h2>\n                                                <p>6 μήνες</p>\n                                                <p>Απεριόριστες επισκέψεις</p>\n                                            </ion-col>\n                                            <ion-col col-4>\n                                                <span class="price">504€</span>\n                                            </ion-col>\n                                        </ion-row>\n                                    </div>\n                                </button >\n                                <button ion-item no-padding (click)="gotoproduct(34,\'864\',\'Presonal Trainning\',\'12 month\' )">\n                                    <div class="Yoga">\n                                        <ion-row>\n                                            <ion-col col-8>\n                                                <h2>Personal Training</h2>\n                                                <p>12 μήνες</p>\n                                                <p>Απεριόριστες επισκέψεις</p>\n                                            </ion-col>\n                                            <ion-col col-4>\n                                                <span class="price">864€</span>\n                                            </ion-col>\n                                        </ion-row>\n                                    </div>\n                                </button >\n\n\n\n                            </ion-list>\n\n                    </ion-col>\n\n                </ion-row>\n            </ion-grid>\n\n        </ion-col>\n\n    </ion-row>\n</ion-grid>\n\n\n<!-- Νεα Πακέτα Credits -->\n\n<ion-grid class="headrow">\n\n  <ion-row>\n  \n    <ion-col col-lg-12><h3 class="dividertitle">Προσθήκη Πακέτου σε Χρήστη</h3>\n\n        <ion-grid>\n            <ion-row>\n\n                <ion-col col-lg-3><h5>Small Packet</h5>\n\n                    <ion-list>\n\n                        <button ion-item (click)="gotoproduct(51,\'50\',\'Small Packet\',\'1 μήνας\' )">\n\n                            <ion-grid>\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Small Packet 1 μήνας</h2>\n                                        <p>60 Credits</p>\n                                        <p>Διάρκεια 1 μήνας</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">50€</span>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>     \n\n                        </button>\n\n\n\n                        <button ion-item (click)="gotoproduct(52,\'120\',\'Small Packet\',\'3 μήνες\' )">\n\n                                <ion-grid>\n                                    <ion-row>\n                                        <ion-col col-8>\n                                            <h2>Small Packet 3 μήνες</h2>\n                                            <p>180 Credits</p>\n                                            <p>Διάρκεια 3 μήνες</p>\n                                            <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                        </ion-col>\n\n                                        <ion-col col-4>\n                                            <span class="price">120€</span>\n                                        </ion-col>\n                                    </ion-row>\n                                </ion-grid>     \n\n                        </button>\n\n\n\n                        <button ion-item (click)="gotoproduct(53,\'210\',\'Small Packet \',\'6 μήνες\' )">\n\n                            <ion-grid>\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Small Packet 6 μήνες</h2>\n                                        <p>360 Credits</p>\n                                        <p>Διάρκεια 6 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">210€</span>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>     \n\n                        </button>\n\n\n                        <button ion-item (click)="gotoproduct(54,\'360\',\'Small Packet \',\'12 μήνες\' )">\n\n                            <ion-grid>\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Small Packet 12 μήνες</h2>\n                                        <p>720 Credits</p>\n                                        <p>Διάρκεια 12 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">360€</span>\n                                    </ion-col>\n                                </ion-row>\n                            </ion-grid>     \n\n                        </button>\n                    </ion-list>\n                </ion-col>\n                \n                <ion-col col-lg-3>\n                    <h5>Medium Packet</h5>\n                    <ion-list padding>\n                        <button ion-item no-padding (click)="gotoproduct(61,\'96\',\'Medium Packet\',\'1 μήνας\' )">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Medium Packet 1 μήνας</h2>\n                                        <p>150 credits</p>\n                                        <p>Διάρκεια 1 μήνας</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n                                    <ion-col col-4>\n                                        <span class="price">96€</span>\n                                    </ion-col>\n                                </ion-row>\n                            </div>\n                        </button >\n                        \n                        <button ion-item no-padding (click)="gotoproduct(62,\'230\',\'Medium Packet\',\'3 μήνες\')">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Medium Packet 3 μήνες</h2>\n                                        <p>450 credits</p>\n                                        <p>Διάρκεια 3 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">230€</span>\n                                    </ion-col>\n\n                                </ion-row>\n                            </div>\n                        </button >\n\n                        <button ion-item no-padding (click)="gotoproduct(63,\'403\',\'Medium Packet\',\'6 μήνες\')">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Medium Packet 6 μήνες</h2>\n                                        <p>900 credits</p>\n                                        <p>Διάρκεια 6 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">403€</span>\n                                    </ion-col>\n\n                                </ion-row>\n                            </div>\n                        </button>\n                        \n                        <button ion-item no-padding (click)="gotoproduct(64,\'691\',\'Medium Packet\',\'12 μήνες\')">\n                           <div class="Yoga">\n                               <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Medium Packet 12 μήνες</h2>\n                                        <p>1800 credits</p>\n                                        <p>Διάρκεια 12 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group</p>\n                                    </ion-col>\n\n                                    <ion-col col-4><span class="price">691€</span></ion-col>\n                               \n                               </ion-row>\n                           </div>\n                          </button >\n\n\n                    </ion-list>\n\n                </ion-col>\n\n\n            <ion-col col-lg-3>\n                    <h5>Large Packet</h5>\n                    <ion-list padding>\n                        <button ion-item no-padding (click)="gotoproduct(71,\'150\',\'Large Packet\',\'1 μήνας\' )">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Large Packet 1 μήνας</h2>\n                                        <p>150 credits</p>\n                                        <p>Διάρκεια 1 μήνας</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group, Pilates Mat, Yoga</p>\n                                    </ion-col>\n                                    <ion-col col-4>\n                                        <span class="price">150€</span>\n                                    </ion-col>\n                                </ion-row>\n                            </div>\n                        </button >\n                        \n                        <button ion-item no-padding (click)="gotoproduct(72,\'360\',\'Large Packet\',\'3 μήνες\')">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Large Packet 3 μήνες</h2>\n                                        <p>450 credits</p>\n                                        <p>Διάρκεια 3 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group, Pilates Mat, Yoga</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">360€</span>\n                                    </ion-col>\n\n                                </ion-row>\n                            </div>\n                        </button >\n\n                        <button ion-item no-padding (click)="gotoproduct(73,\'630\',\'Large Packet\',\'6 μήνες\')">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Large Packet 6 μήνες</h2>\n                                        <p>900 credits</p>\n                                        <p>Διάρκεια 6 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group, Pilates Mat, Yoga</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">630€</span>\n                                    </ion-col>\n\n                                </ion-row>\n                            </div>\n                        </button>\n                        \n                        <button ion-item no-padding (click)="gotoproduct(74,\'1080\',\'Large Packet\',\'12 μήνες\')">\n                           <div class="Yoga">\n                               <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Large Packet 12 μήνες</h2>\n                                        <p>1800 credits</p>\n                                        <p>Διάρκεια 12 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Απεριόριστα Group, Pilates Mat, Yoga</p>\n                                    </ion-col>\n\n                                    <ion-col col-4><span class="price">1080€</span></ion-col>\n                               \n                               </ion-row>\n                           </div>\n                          </button >\n\n\n                    </ion-list>\n\n                </ion-col>\n\n            <ion-col col-lg-3>\n                    <h5>Premium Packet</h5>\n                    <ion-list padding>\n                        <button ion-item no-padding (click)="gotoproduct(81,\'249\',\'Premium Packet\',\'1 μήνας\' )">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Premium Packet 1 μήνας</h2>\n                                        <p>240 credits</p>\n                                        <p>Διάρκεια 1 μήνας</p>\n                                        <p>Όλα τα προγράμματα - Όλα Απεριόριστα εκτός Private</p>\n                                    </ion-col>\n                                    <ion-col col-4>\n                                        <span class="price">249€</span>\n                                    </ion-col>\n                                </ion-row>\n                            </div>\n                        </button >\n                        \n                        <button ion-item no-padding (click)="gotoproduct(82,\'598\',\'Premium Packet\',\'3 μήνες\')">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Premium Packet 3 μήνες</h2>\n                                        <p>720 credits</p>\n                                        <p>Διάρκεια 3 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Όλα Απεριόριστα εκτός Private</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">598€</span>\n                                    </ion-col>\n\n                                </ion-row>\n                            </div>\n                        </button >\n\n                        <button ion-item no-padding (click)="gotoproduct(83,\'1046\',\'Premium Packet\',\'6 μήνες\')">\n                            <div class="Yoga">\n                                <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Premium Packet 6 μήνες</h2>\n                                        <p>1440 credits</p>\n                                        <p>Διάρκεια 6 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Όλα Απεριόριστα εκτός Private</p>\n                                    </ion-col>\n\n                                    <ion-col col-4>\n                                        <span class="price">1046€</span>\n                                    </ion-col>\n\n                                </ion-row>\n                            </div>\n                        </button>\n                        \n                        <button ion-item no-padding (click)="gotoproduct(84,\'1793\',\'Premium Packet\',\'12 μήνες\')">\n                           <div class="Yoga">\n                               <ion-row>\n                                    <ion-col col-8>\n                                        <h2>Premium Packet 12 μήνες</h2>\n                                        <p>2880 credits</p>\n                                        <p>Διάρκεια 12 μήνες</p>\n                                        <p>Όλα τα προγράμματα - Όλα Απεριόριστα εκτός Private</p>\n                                    </ion-col>\n\n                                    <ion-col col-4><span class="price">1793€</span></ion-col>\n                               \n                               </ion-row>\n                           </div>\n                          </button >\n\n\n                    </ion-list>\n\n                </ion-col>\n\n          </ion-row>\n        </ion-grid>\n\n    </ion-col>\n\n  </ion-row>\n</ion-grid>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/customerview/customerview.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], CustomerviewPage);
    return CustomerviewPage;
    var CustomerviewPage_1;
}());

//# sourceMappingURL=customerview.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__trainerhome_trainerhome__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, storage, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.http = http;
    }
    LoginPage.prototype.gologin = function () {
        var _this = this;
        var url = 'https://zonepage.gr/gymapp/appcrm/userlogin.php';
        var postData = new FormData();
        postData.append('login', '');
        postData.append('email', this.username);
        postData.append('password', this.password);
        this.data = this.http.post(url, postData);
        this.data.subscribe(function (data) {
            if (data == "error") {
            }
            else {
                if (data.useraccess == "2") {
                    _this.storage.set('crmid', data);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */]);
                }
                else {
                    _this.storage.set('crmid', data);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__trainerhome_trainerhome__["a" /* TrainerhomePage */]);
                }
            }
            console.log(data.useraccess);
        });
        //this.navCtrl.push(HomePage);
    };
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n\n\n</ion-header>\n\n\n<ion-content padding>\n\n<div class="loginform">\n\n<ion-list>\n\n  <ion-item class="form">\n    <ion-label >Username</ion-label>\n    <ion-input type="text" [(ngModel)]="username" required></ion-input>\n  </ion-item>\n\n  <ion-item class="form">\n    <ion-label>Password</ion-label>\n    <ion-input type="password" [(ngModel)]="password" required></ion-input>\n  </ion-item>\n\n</ion-list>\n\n<div padding>\n<button ion-button block outline (click)="gologin()">Login</button>\n</div>\n\n</div>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 770:
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ 822:
/***/ (function(module, exports) {

module.exports = function(module) {
	if(!module.webpackPolyfill) {
		module.deprecate = function() {};
		module.paths = [];
		// module.parent = undefined by default
		if(!module.children) module.children = [];
		Object.defineProperty(module, "loaded", {
			enumerable: true,
			get: function() {
				return module.l;
			}
		});
		Object.defineProperty(module, "id", {
			enumerable: true,
			get: function() {
				return module.i;
			}
		});
		module.webpackPolyfill = 1;
	}
	return module;
};


/***/ }),

/***/ 823:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 336,
	"./af.js": 336,
	"./ar": 337,
	"./ar-dz": 338,
	"./ar-dz.js": 338,
	"./ar-kw": 339,
	"./ar-kw.js": 339,
	"./ar-ly": 340,
	"./ar-ly.js": 340,
	"./ar-ma": 341,
	"./ar-ma.js": 341,
	"./ar-sa": 342,
	"./ar-sa.js": 342,
	"./ar-tn": 343,
	"./ar-tn.js": 343,
	"./ar.js": 337,
	"./az": 344,
	"./az.js": 344,
	"./be": 345,
	"./be.js": 345,
	"./bg": 346,
	"./bg.js": 346,
	"./bm": 347,
	"./bm.js": 347,
	"./bn": 348,
	"./bn.js": 348,
	"./bo": 349,
	"./bo.js": 349,
	"./br": 350,
	"./br.js": 350,
	"./bs": 351,
	"./bs.js": 351,
	"./ca": 352,
	"./ca.js": 352,
	"./cs": 353,
	"./cs.js": 353,
	"./cv": 354,
	"./cv.js": 354,
	"./cy": 355,
	"./cy.js": 355,
	"./da": 356,
	"./da.js": 356,
	"./de": 357,
	"./de-at": 358,
	"./de-at.js": 358,
	"./de-ch": 359,
	"./de-ch.js": 359,
	"./de.js": 357,
	"./dv": 360,
	"./dv.js": 360,
	"./el": 361,
	"./el.js": 361,
	"./en-SG": 362,
	"./en-SG.js": 362,
	"./en-au": 363,
	"./en-au.js": 363,
	"./en-ca": 364,
	"./en-ca.js": 364,
	"./en-gb": 365,
	"./en-gb.js": 365,
	"./en-ie": 366,
	"./en-ie.js": 366,
	"./en-il": 367,
	"./en-il.js": 367,
	"./en-nz": 368,
	"./en-nz.js": 368,
	"./eo": 369,
	"./eo.js": 369,
	"./es": 370,
	"./es-do": 371,
	"./es-do.js": 371,
	"./es-us": 372,
	"./es-us.js": 372,
	"./es.js": 370,
	"./et": 373,
	"./et.js": 373,
	"./eu": 374,
	"./eu.js": 374,
	"./fa": 375,
	"./fa.js": 375,
	"./fi": 376,
	"./fi.js": 376,
	"./fo": 377,
	"./fo.js": 377,
	"./fr": 378,
	"./fr-ca": 379,
	"./fr-ca.js": 379,
	"./fr-ch": 380,
	"./fr-ch.js": 380,
	"./fr.js": 378,
	"./fy": 381,
	"./fy.js": 381,
	"./ga": 382,
	"./ga.js": 382,
	"./gd": 383,
	"./gd.js": 383,
	"./gl": 384,
	"./gl.js": 384,
	"./gom-latn": 385,
	"./gom-latn.js": 385,
	"./gu": 386,
	"./gu.js": 386,
	"./he": 387,
	"./he.js": 387,
	"./hi": 388,
	"./hi.js": 388,
	"./hr": 389,
	"./hr.js": 389,
	"./hu": 390,
	"./hu.js": 390,
	"./hy-am": 391,
	"./hy-am.js": 391,
	"./id": 392,
	"./id.js": 392,
	"./is": 393,
	"./is.js": 393,
	"./it": 394,
	"./it-ch": 395,
	"./it-ch.js": 395,
	"./it.js": 394,
	"./ja": 396,
	"./ja.js": 396,
	"./jv": 397,
	"./jv.js": 397,
	"./ka": 398,
	"./ka.js": 398,
	"./kk": 399,
	"./kk.js": 399,
	"./km": 400,
	"./km.js": 400,
	"./kn": 401,
	"./kn.js": 401,
	"./ko": 402,
	"./ko.js": 402,
	"./ku": 403,
	"./ku.js": 403,
	"./ky": 404,
	"./ky.js": 404,
	"./lb": 405,
	"./lb.js": 405,
	"./lo": 406,
	"./lo.js": 406,
	"./lt": 407,
	"./lt.js": 407,
	"./lv": 408,
	"./lv.js": 408,
	"./me": 409,
	"./me.js": 409,
	"./mi": 410,
	"./mi.js": 410,
	"./mk": 411,
	"./mk.js": 411,
	"./ml": 412,
	"./ml.js": 412,
	"./mn": 413,
	"./mn.js": 413,
	"./mr": 414,
	"./mr.js": 414,
	"./ms": 415,
	"./ms-my": 416,
	"./ms-my.js": 416,
	"./ms.js": 415,
	"./mt": 417,
	"./mt.js": 417,
	"./my": 418,
	"./my.js": 418,
	"./nb": 419,
	"./nb.js": 419,
	"./ne": 420,
	"./ne.js": 420,
	"./nl": 421,
	"./nl-be": 422,
	"./nl-be.js": 422,
	"./nl.js": 421,
	"./nn": 423,
	"./nn.js": 423,
	"./pa-in": 424,
	"./pa-in.js": 424,
	"./pl": 425,
	"./pl.js": 425,
	"./pt": 426,
	"./pt-br": 427,
	"./pt-br.js": 427,
	"./pt.js": 426,
	"./ro": 428,
	"./ro.js": 428,
	"./ru": 429,
	"./ru.js": 429,
	"./sd": 430,
	"./sd.js": 430,
	"./se": 431,
	"./se.js": 431,
	"./si": 432,
	"./si.js": 432,
	"./sk": 433,
	"./sk.js": 433,
	"./sl": 434,
	"./sl.js": 434,
	"./sq": 435,
	"./sq.js": 435,
	"./sr": 436,
	"./sr-cyrl": 437,
	"./sr-cyrl.js": 437,
	"./sr.js": 436,
	"./ss": 438,
	"./ss.js": 438,
	"./sv": 439,
	"./sv.js": 439,
	"./sw": 440,
	"./sw.js": 440,
	"./ta": 441,
	"./ta.js": 441,
	"./te": 442,
	"./te.js": 442,
	"./tet": 443,
	"./tet.js": 443,
	"./tg": 444,
	"./tg.js": 444,
	"./th": 445,
	"./th.js": 445,
	"./tl-ph": 446,
	"./tl-ph.js": 446,
	"./tlh": 447,
	"./tlh.js": 447,
	"./tr": 448,
	"./tr.js": 448,
	"./tzl": 449,
	"./tzl.js": 449,
	"./tzm": 450,
	"./tzm-latn": 451,
	"./tzm-latn.js": 451,
	"./tzm.js": 450,
	"./ug-cn": 452,
	"./ug-cn.js": 452,
	"./uk": 453,
	"./uk.js": 453,
	"./ur": 454,
	"./ur.js": 454,
	"./uz": 455,
	"./uz-latn": 456,
	"./uz-latn.js": 456,
	"./uz.js": 455,
	"./vi": 457,
	"./vi.js": 457,
	"./x-pseudo": 458,
	"./x-pseudo.js": 458,
	"./yo": 459,
	"./yo.js": 459,
	"./zh-cn": 460,
	"./zh-cn.js": 460,
	"./zh-hk": 461,
	"./zh-hk.js": 461,
	"./zh-tw": 462,
	"./zh-tw.js": 462
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 823;

/***/ }),

/***/ 845:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(506);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(507);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_customers_customers__ = __webpack_require__(68);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_weeklyprograms_weeklyprograms__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_classes_classes__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_orders_orders__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_home_home__ = __webpack_require__(141);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_login_login__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_invoices_invoices__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_dateshistory_dateshistory__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_cutomersstats_cutomersstats__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_allexpires_allexpires__ = __webpack_require__(508);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_traines_priva_traines_priva__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_watersbuy_watersbuy__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_trainerhome_trainerhome__ = __webpack_require__(95);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, storage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.storage = storage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */];
        this.storage.get('crmid').then(function (val) {
            console.log('Your age is', val);
            if (val == null) {
                _this.rootPage = __WEBPACK_IMPORTED_MODULE_10__pages_login_login__["a" /* LoginPage */];
            }
            else {
                _this.userid = val.useraccess;
                if (_this.userid == "2") {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */];
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_17__pages_trainerhome_trainerhome__["a" /* TrainerhomePage */];
                }
            }
        });
        this.initializeApp();
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'Home', component: __WEBPACK_IMPORTED_MODULE_9__pages_home_home__["a" /* HomePage */] },
            { title: 'Πελάτες', component: __WEBPACK_IMPORTED_MODULE_4__pages_customers_customers__["a" /* CustomersPage */] },
            { title: 'Ραντεβού Ιστορικό', component: __WEBPACK_IMPORTED_MODULE_12__pages_dateshistory_dateshistory__["a" /* DateshistoryPage */] },
            { title: 'Ληγμένα Προγράμματα', component: __WEBPACK_IMPORTED_MODULE_14__pages_allexpires_allexpires__["a" /* AllexpiresPage */] },
            { title: 'Στατιστικά', component: __WEBPACK_IMPORTED_MODULE_13__pages_cutomersstats_cutomersstats__["a" /* CutomersstatsPage */] },
            { title: 'Classes', component: __WEBPACK_IMPORTED_MODULE_6__pages_classes_classes__["a" /* ClassesPage */] },
            { title: 'Weekly Program', component: __WEBPACK_IMPORTED_MODULE_5__pages_weeklyprograms_weeklyprograms__["a" /* WeeklyprogramsPage */] },
            { title: 'Invoices', component: __WEBPACK_IMPORTED_MODULE_11__pages_invoices_invoices__["a" /* InvoicesPage */] },
            { title: 'Trainers Stats', component: __WEBPACK_IMPORTED_MODULE_15__pages_traines_priva_traines_priva__["a" /* TrainesPrivaPage */] },
            { title: 'Waters', component: __WEBPACK_IMPORTED_MODULE_16__pages_watersbuy_watersbuy__["a" /* WatersbuyPage */] },
            { title: 'Orders', component: __WEBPACK_IMPORTED_MODULE_7__pages_orders_orders__["a" /* OrdersPage */] },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/app/app.html"*/'<ion-menu [content]="content">\n  <ion-header>\n    <ion-toolbar>\n      <ion-title>Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content>\n    <ion-list>\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n        {{p.title}}\n      </button>\n    </ion-list>\n  </ion-content>\n\n</ion-menu>\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n<ion-nav class="admin" [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["b" /* Storage */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 846:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ListPage = /** @class */ (function () {
    function ListPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        // If we navigated to this page, we will have an item available as a nav param
        this.selectedItem = navParams.get('item');
        // Let's populate this page with some filler content for funzies
        this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
            'american-football', 'boat', 'bluetooth', 'build'];
        this.items = [];
        for (var i = 1; i < 11; i++) {
            this.items.push({
                title: 'Item ' + i,
                note: 'This is item #' + i,
                icon: this.icons[Math.floor(Math.random() * this.icons.length)]
            });
        }
    }
    ListPage_1 = ListPage;
    ListPage.prototype.itemTapped = function (event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(ListPage_1, {
            item: item
        });
    };
    ListPage = ListPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/list/list.html"*/'<ion-header>\n  <ion-navbar>\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>List</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <button ion-item *ngFor="let item of items" (click)="itemTapped($event, item)">\n      <ion-icon [name]="item.icon" item-start></ion-icon>\n      {{item.title}}\n      <div class="item-note" item-end>\n        {{item.note}}\n      </div>\n    </button>\n  </ion-list>\n  <div *ngIf="selectedItem" padding>\n    You navigated here from <b>{{selectedItem.title}}</b>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/list/list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */]])
    ], ListPage);
    return ListPage;
    var ListPage_1;
}());

//# sourceMappingURL=list.js.map

/***/ }),

/***/ 94:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrdersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__vieworder_vieworder__ = __webpack_require__(180);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the OrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var OrdersPage = /** @class */ (function () {
    function OrdersPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.getorders();
    }
    OrdersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad OrdersPage');
    };
    OrdersPage.prototype.getorders = function () {
        var _this = this;
        var data;
        var url = 'http://zonepage.gr/gymapp/appcrm/getorders.php';
        var postData = new FormData();
        postData.append('status', 'get');
        data = this.http.post(url, postData);
        data.subscribe(function (data) {
            _this.items = data;
            console.log(data);
            //this.storage.set('id', data);		 
            //console.log (this.items[category]);
        });
    };
    OrdersPage.prototype.gotoproduct = function (item) {
        console.log(item.idpro);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__vieworder_vieworder__["a" /* VieworderPage */], { item: item });
    };
    OrdersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-orders',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/orders/orders.html"*/'<!--\n  Generated template for the OrdersPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n   <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>orders</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n <ion-list>\n    <ion-item *ngFor="let item of items" (click)="gotoproduct(item)">\n	<ion-grid>\n  <ion-row>\n	<ion-col col-2>\n      <h2>#{{item.orderid}}</h2>\n	 \n   </ion-col>\n	\n    <ion-col col-6><h2>{{item.fullname}} {{item.lastname}} - {{item.title}}</h2>\n	  <p>{{item.created}}</p>\n	  <p>Κόστος {{item.amount}}</p> <p *ngIf="item.credits > \'0\'">Custom Program Credits {{item.credits}}</p></ion-col>\n\n    <ion-col col-4 *ngIf="item.orderstatus === \'0\'"><p class="price">On Going</p>\n	</ion-col>\n	 <ion-col col-4 *ngIf="item.orderstatus === \'1\'"><p class="price">Deleted</p>\n	</ion-col>\n	 <ion-col col-4 *ngIf="item.orderstatus === \'2\'"><p class="price">Complete</p>\n	</ion-col>\n  </ion-row>\n</ion-grid>\n	\n	</ion-item>\n	\n	</ion-list>\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/orders/orders.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], OrdersPage);
    return OrdersPage;
}());

//# sourceMappingURL=orders.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TrainerhomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__login_login__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__sendzoom_sendzoom__ = __webpack_require__(142);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};








/**
 * Generated class for the TrainerhomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var TrainerhomePage = /** @class */ (function () {
    function TrainerhomePage(navCtrl, navParams, http, storage, iab, modalCtrl, menu) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.storage = storage;
        this.iab = iab;
        this.modalCtrl = modalCtrl;
        this.menu = menu;
        this.nowdate = new Date().toISOString();
        this.menu.enable(false);
        this.storage.get('crmid').then(function (val) {
            _this.userid = val.trainerid;
            console.log(_this.userid);
        });
        this.daylist = 1;
        this.sub = __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].interval(300000)
            .subscribe(function (val) {
            console.log('called');
            _this.getusercome(_this.daylist);
        });
    }
    TrainerhomePage.prototype.getusercome = function (value) {
        var _this = this;
        console.log(value);
        var data;
        var url = 'https://zonepage.gr/gymapp/appcrm/cominguserstrainer.php';
        var postData = new FormData();
        postData.append('daylist', this.daylist);
        postData.append('userid', this.userid);
        data = this.http.post(url, postData);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    TrainerhomePage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad TrainerhomePage');
        this.getusercome(this.daylist);
    };
    TrainerhomePage.prototype.gologout = function () {
        this.storage.clear();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__login_login__["a" /* LoginPage */]);
    };
    TrainerhomePage.prototype.gotoprogram = function (item) {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var text, email, profileModal;
            return __generator(this, function (_a) {
                text = "test";
                console.log(text);
                email = item.email;
                profileModal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_7__sendzoom_sendzoom__["a" /* SendzoomPage */], { userId: email, reg_id: item.reg_id, link: text });
                profileModal.onDidDismiss(function (data) {
                    console.log(data);
                    _this.madalDismissData = JSON.stringify(data);
                });
                profileModal.present();
                return [2 /*return*/];
            });
        });
    };
    TrainerhomePage.prototype.openModal = function () {
    };
    TrainerhomePage.prototype.onChange = function (value) {
        var _this = this;
        this.daylist = value;
        this.sub.unsubscribe();
        this.getusercome(this.daylist);
        this.sub = __WEBPACK_IMPORTED_MODULE_4_rxjs_Observable__["Observable"].interval(300000)
            .subscribe(function (val) {
            console.log('called');
            _this.getusercome(_this.daylist);
        });
    };
    TrainerhomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-trainerhome',template:/*ion-inline-start:"/home/nikos/Documents/mendezerp/src/pages/trainerhome/trainerhome.html"*/'<!--\n  Generated template for the TrainerhomePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Traine Home</ion-title>\n    \n	<ion-buttons end>\n<button ion-button outline end (click)="gologout()">Logout</button>\n\n</ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n	<ion-grid>\n  <ion-row>\n    <ion-col col-lg-6>\n\n    <h2>Ραντεβού</h2> \n     <ion-label>Filter</ion-label>\n    <ion-select [(ngModel)]="toppings" multiple="false" cancelText="Cancel" okText="Ok" (ionChange)="onChange($event)">\n      <ion-option value = "1" selected>Τελευταία Ραντεβού</ion-option>\n      <ion-option value = "2">Ραντεβού Σήμερα</ion-option>  \n    </ion-select>\n   \n\n	<p>Γίνεται αναναίωση κάθε 5 λεπτά</p>\n	 <ion-list>\n\n <ion-item *ngFor="let item of users" (click)="gotoprogram(item)">\n    <h3>{{item.fullname}}  {{item.lastname}}</h3> \n    <h1>{{item.email}}</h1>\n	<p>{{item.tilte}} - Trainer: {{item.person}}</p>\n	<p>{{item.timehour1}}</p>\n	<p>Σχόλιο : {{item.comment}}</p>\n    </ion-item> \n\n\n\n</ion-list>\n    </ion-col >\n\n</ion-row>\n</ion-grid>\n\n\n</ion-content>\n'/*ion-inline-end:"/home/nikos/Documents/mendezerp/src/pages/trainerhome/trainerhome.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* MenuController */]])
    ], TrainerhomePage);
    return TrainerhomePage;
}());

//# sourceMappingURL=trainerhome.js.map

/***/ })

},[512]);
//# sourceMappingURL=main.js.map